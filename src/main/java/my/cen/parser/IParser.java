package my.cen.parser;

import java.util.Map;

/**
 * Created  on 9/5/2015.
 */
public interface IParser {

    void parse(Map<String, Object> map);
}
