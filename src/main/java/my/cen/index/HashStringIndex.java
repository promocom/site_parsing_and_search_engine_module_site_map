package my.cen.index;

import org.elasticsearch.common.base.Throwables;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Defines contract for the index classes
 *
 * @author Eugene Chyrski
 * @since 2015-09-04 02:41:16
 */
public class HashStringIndex extends StringIndex {
    private static final String DIGEST_PROVIDER = "MD5";

    public HashStringIndex(File file)  {
        super(file);
    }

    public HashStringIndex(File file, byte pageSize) {
        super(file, pageSize);
    }

    public boolean searchOrIndex(final String term) {
        String hash = hash(term);
        return super.searchOrIndex(hash);
    }

    protected String hash(String in) {
        try {

            MessageDigest digest = MessageDigest.getInstance(DIGEST_PROVIDER);
            return new BigInteger(1, digest.digest(in.getBytes())).toString(Character.MAX_RADIX);
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }
}
