package my.cen.config;

/**
 * Created  on 9/16/2015.
 */
public class ConfigConstants {
    public static final String DEBUG_ENV = "debug";
    public static final String HOME_FOLDER = "home.folder";
    public static final String USE_TOR = "tor.use";
    public static final String TOR_HOST = "tor.host";
    public static final String TOR_PORT = "tor.port";
    public static final String DISCOVERY_THREADS = "discovery.threads";
    public static final String CONCURRENT_SITES = "concurrent.sites";
    public static final String CONCURRENT_PAGES = "concurrent.pages";
    public static final String PAGE_PER_THREAD = "pages.per.thread";

    public static final String ES_DISABLE_DYNAMIC = "script.disable_dynamic";
    public static final String ES_DATA_PATH = "path.data";

}
