package my.cen.config;

import groovy.lang.GroovyClassLoader;
import my.cen.discovery.DiscoveryThread;
import my.cen.geo.GeoCoderThread;
import my.cen.geo.GeoObject;
import my.cen.model.spider.SiteParsingOptions;
import my.cen.model.spider.SiteParsingOptionsHolder;
import my.cen.model.spider.content.SiteMapSpiderSubmitter;
import my.cen.parser.IParser;
import my.cen.service.ElasticSearchService;
import my.cen.service.StatisticsService;
import my.cen.service.impl.ElasticSearchServiceImpl;
import my.cen.service.impl.StatisticsServiceImpl;
import my.cen.service.storage.impl.helpers.FileRemoveThread;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.collect.ImmutableList;
import org.elasticsearch.common.collect.ImmutableMap;
import org.elasticsearch.common.util.concurrent.ThreadFactoryBuilder;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static my.cen.config.ConfigConstants.*;

/**
 * Created by PC on 15.12.2014.
 */
@Configuration
@PropertySource({
        //last one wins here
        "classpath:main.properties",
        "classpath:env/${env.name:" + DEBUG_ENV + "}.properties"

})
public class MainConfig {

    private static final Logger logger = Logger.getLogger(MainConfig.class);
    @Autowired
    protected Environment env;


    @PostConstruct
    public void init() {
        if (Boolean.parseBoolean(env.getProperty(USE_TOR))) {
            System.setProperty("socksProxyHost", env.getProperty(TOR_HOST));
            System.setProperty("socksProxyPort", env.getProperty(TOR_PORT));
        }

    }


    private Node node;

    @Bean
    public Client elasticSearchClient() {
        if (node == null) {
            NodeBuilder builder = NodeBuilder.nodeBuilder();
            builder.settings().put(ImmutableMap.of(ES_DATA_PATH, env.getProperty(HOME_FOLDER) + "data", ES_DISABLE_DYNAMIC, "false"));
            node = builder.client(false).node();
        }
        return node.client();
    }

    @Bean
    public ElasticSearchService elasticSearchService(Client elasticSearchClient) {
        ElasticSearchServiceImpl res = new ElasticSearchServiceImpl(elasticSearchClient);
        res.optimize();
        return res;
    }


    @Bean
    public ExecutorService mainExecutor(SiteParsingOptionsHolder options, ElasticSearchService elasticSearchService, StatisticsService stat) {
        int concurrentSites = Integer.parseInt(env.getProperty(CONCURRENT_SITES));
        ThreadPoolExecutor executor = new ThreadPoolExecutor(concurrentSites, concurrentSites, 1L, TimeUnit.MINUTES, new LinkedBlockingDeque<>(300));

        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("submitter-%d").build();
        executor.setThreadFactory(namedThreadFactory);
        FileRemoveThread fileRemoveThread = new FileRemoveThread();
        fileRemoveThread.setName("file-remove-thread");
        fileRemoveThread.start();
        DiscoveryThread discoveryThread = new DiscoveryThread(env.getProperty(HOME_FOLDER), Integer.parseInt(env.getProperty(DISCOVERY_THREADS)));
        discoveryThread.setName("Discovery thread");
        discoveryThread.start();
        final Collection<GeoObject> geoObjects = new HashSet<>();
        try {
            List<String> lines = IOUtils.readLines(this.getClass().getClassLoader().getResourceAsStream("cities.geo"));
             lines.forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    String split[] = s.split(";", 3);
                    if (split.length == 3) {

                        geoObjects.add(new GeoObject(split[0], split[1], split[2].replace(";", ",")));
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        geoObjects.remove(null);
        GeoCoderThread geoCoderThread = new GeoCoderThread(geoObjects, elasticSearchService);
        geoCoderThread.setName("GeoCoder thread");
        geoCoderThread.start();
        executor.execute(new SiteMapSpiderSubmitter(env.getProperty(HOME_FOLDER), options, executor, elasticSearchService, stat));
        return executor;
    }


    @Bean
    public SiteParsingOptionsHolder parsingOptionsList() {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(this.getClass().getClassLoader().getResourceAsStream("my/cen/config/template.groovy"), writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String spiderSitesPath = env.getProperty(HOME_FOLDER) + "spiderSites/";
        ;
        SiteParsingOptionsHolder options = new SiteParsingOptionsHolder(writer.toString(), spiderSitesPath, Integer.parseInt(env.getProperty(CONCURRENT_PAGES)),
                Integer.parseInt(env.getProperty(PAGE_PER_THREAD)));

        File settingsFolder = new File(spiderSitesPath);
        String[] files = settingsFolder.list();
        if (files == null) return options;
        for (String file : files) {
            options.load(new File(spiderSitesPath + file));
        }
        return options;
    }

    @Bean
    public StatisticsService statisticsService() {
        return new StatisticsServiceImpl(21);
    }
}
