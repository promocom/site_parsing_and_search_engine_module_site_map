package my.cen.model.spider.http;

import org.jsoup.nodes.Document;

import java.util.Map;

/**
 * Created by PC on 28.11.2014.
 */
public class HttpResponse {
    private final int respCode;

    private final Map<String, String> headers;
    private final Document documet;
    private final HttpResponse followedResponse;

    public HttpResponse(HttpResponse followedResponse, int respCode, Map<String, String> headers, Document document) {
        this.followedResponse = followedResponse;
        this.respCode = respCode;
        this.headers = headers;
        this.documet = document;
    }

    public HttpResponse(HttpResponse followedResponse, int respCode, Map<String, String> headers) {
        this(followedResponse, respCode, headers, null);
    }

    public HttpResponse(int respCode, Map<String, String> headers, Document document) {
        this(null, respCode, headers, document);
    }

    public int getRespCode() {
        return respCode;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Document getDocumet() {
        return documet;
    }

    public HttpResponse getFollowedResponse() {
        return followedResponse;
    }
}
