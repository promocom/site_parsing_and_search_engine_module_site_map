package my.cen.model.spider.content;

import groovy.lang.GroovyClassLoader;
import my.cen.model.spider.ISiteParsingOptionsHolder;
import my.cen.model.spider.SiteParsingOptions;
import my.cen.parser.IParser;
import my.cen.service.ElasticSearchService;
import my.cen.service.SiteState;
import my.cen.service.StatisticsService;
import my.cen.service.storage.impl.helpers.FileRemoveThread;
import my.cen.utils.ThreadUtils;
import net.freehaven.tor.control.NewNym;
import org.apache.log4j.Logger;
import org.elasticsearch.common.base.Throwables;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created  on 9/8/2015.
 */
public class SiteMapSpiderSubmitter extends Thread {

    private final static Logger logger = Logger.getLogger(SiteMapSpider.class);
    private final ISiteParsingOptionsHolder options;
    private final ExecutorService executor;

    private final String path;
    private final ElasticSearchService ess;
    private final StatisticsService stat;

    public SiteMapSpiderSubmitter(String path, ISiteParsingOptionsHolder options, ExecutorService executor, ElasticSearchService ess, StatisticsService stat) {
        super();
        this.options = options;
        this.executor = executor;

        this.path = path;
        this.ess = ess;
        this.stat = stat;


    }

    public void run() {
        Map<String, Long> zeroIndexedSites = new HashMap<>();

        while (true) {
            newNym();
            stat.setTotalProcessed(ess.count());
            stat.setTotalRemoved(FileRemoveThread.deleted.longValue());
            List<Future<SiteMapSpiderResult>> futures = new ArrayList<>();
            for (SiteParsingOptions option : options.allOptions()) {
                Long lastCheck = zeroIndexedSites.get(option.getSiteKey());
                if (lastCheck == null || System.currentTimeMillis() - lastCheck > TimeUnit.MINUTES.toMillis(25)) {
                    zeroIndexedSites.remove(option.getSiteKey());
                    futures.add(executor.submit(new SiteMapSpider(path, option, ess, option.getParser(),stat)));
                    stat.setSiteState(option.getSiteKey(), SiteState.processing);
                }
            }
            try {
                List<Future<SiteMapSpiderResult>> futuresAwaiting = new ArrayList<>();
                while (futures.size() > 0) {
                    ThreadUtils.sleepNoInterrupt(TimeUnit.SECONDS.toMillis(10));
                    for (Future<SiteMapSpiderResult> result : futures) {
                        if (!result.isCancelled() && !result.isDone()) {
                            futuresAwaiting.add(result);
                        } else if (result.isDone()) {
                            try {
                                SiteMapSpiderResult siteMapSpiderResult = result.get();
                                if (siteMapSpiderResult.isHasData()) {
                                    stat.setTotalProcessed(ess.count());
                                    stat.setTotalRemoved(FileRemoveThread.deleted.longValue());
                                    stat.setSiteState(siteMapSpiderResult.getSite(), SiteState.processing);
                                    futuresAwaiting.add(
                                            executor.submit(
                                                    new SiteMapSpider(path,
                                                            getOption(siteMapSpiderResult.getSite()),
                                                            ess,
                                                            getOption(siteMapSpiderResult.getSite()).getParser(),stat)));
                                } else {
                                    stat.setSiteState(siteMapSpiderResult.getSite(), SiteState.awaiting);
                                    zeroIndexedSites.put(siteMapSpiderResult.getSite(), System.currentTimeMillis());

                                }
                            } catch (ExecutionException e) {
                                logger.error("While trying to access complete future result", e);
                            }
                        }

                    }
                    List<String> toRemove = new ArrayList<>();
                    for (Map.Entry<String, Long> emptyIndex : zeroIndexedSites.entrySet()) {
                        if (System.currentTimeMillis() - emptyIndex.getValue() > TimeUnit.MINUTES.toMillis(25)) {
                            toRemove.add(emptyIndex.getKey());
                            SiteParsingOptions option = getOption(emptyIndex.getKey());
                            stat.setSiteState(option.getSiteKey(), SiteState.processing);
                            if (option != null) {
                                futuresAwaiting.add(
                                        executor.submit(
                                                new SiteMapSpider(path,
                                                        option,
                                                        ess,
                                                        option.getParser(),stat)));
                            }
                        }
                    }
                    toRemove.forEach(zeroIndexedSites::remove);
                    futures.clear();
                    futures.addAll(futuresAwaiting);
                    futuresAwaiting.clear();

                }
            } catch (InterruptedException e) {
                logger.error("While executing SiteMapSpider's", e);
            }
            ThreadUtils.sleepNoInterrupt(TimeUnit.SECONDS.toMillis(10));
        }
    }

    private SiteParsingOptions getOption(String site) {
        return options.get(site);
    }


    private void newNym() {
        try {
            NewNym.doNewNym();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
