package my.cen.model.spider.content;

/**
 * Created  on 9/8/2015.
 */
public class SiteMapSpiderResult {
    private final String site;
    private final boolean hasData;

    public SiteMapSpiderResult(String site, boolean hasData) {
        this.site = site;
        this.hasData = hasData;
    }

    public String getSite() {
        return site;
    }

    public boolean isHasData() {
        return hasData;
    }
}
