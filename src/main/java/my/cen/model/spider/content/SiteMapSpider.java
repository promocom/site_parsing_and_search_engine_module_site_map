package my.cen.model.spider.content;

import groovy.lang.GroovyClassLoader;
import my.cen.model.spider.SiteParsingOptions;
import my.cen.parser.IParser;
import my.cen.service.ElasticSearchService;
import my.cen.service.StatisticsService;
import my.cen.service.storage.impl.IndexBasedStorageService;
import my.cen.utils.ThreadUtils;
import net.freehaven.tor.control.NewNym;
import org.apache.log4j.Logger;
import org.elasticsearch.common.base.Throwables;
import org.elasticsearch.common.util.concurrent.ThreadFactoryBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by PC on 22.11.2014.
 */
public class SiteMapSpider implements Callable<SiteMapSpiderResult> {
    private final static Logger logger = Logger.getLogger(SiteMapSpider.class);
    private final SiteParsingOptions option;
    private final IParser parser;

    private final String path;
    private final ElasticSearchService ess;
    private final StatisticsService stat;

    public SiteMapSpider(String path, SiteParsingOptions option, ElasticSearchService ess, IParser parser, StatisticsService stat) {
        this.option = option;
        this.parser = parser;
        this.path = path;
        this.ess = ess;
        this.stat = stat;


    }


    @Override
    public SiteMapSpiderResult call() throws Exception {
        int linksToProcess = option.getPagesPerThread() * option.getThreadCount();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 1L, TimeUnit.MINUTES, new LinkedBlockingDeque<>(300));
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("spider-" + option.getSiteKey() + "-%d").build();
        executor.setThreadFactory(namedThreadFactory);

        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        try (IndexBasedStorageService storageService = new IndexBasedStorageService(ess, path, option.getSiteKey(), option.isHash())) {
            try {
                List<URL> urls = storageService.list(linksToProcess);
                if (urls.size() == 0) {
                    //calling synchronous in order to get new records
                    Future future = executor.submit(
                            new SiteMapSpiderRunnable(
                                    storageService,
                                    option.getSite(),
                                    option,
                                    parser));
                    while (true) {
                        if (future.isCancelled() || future.isDone()) {
                            break;
                        }
                        ThreadUtils.sleepNoInterrupt(100);
                    }
                    urls = storageService.list(linksToProcess);
                    logger.debug(option.getSiteKey() + ":Loaded following url from main url " + urls);
                    if (urls.size() == 0) {
                        return new SiteMapSpiderResult(option.getSiteKey(), false);
                    }
                }
                stat.setSiteIndexCount(option.getSiteKey(), storageService.tempIndexCount());
                int poolSize = (int) Math.max(1, Math.ceil(urls.size() * 1.0 / option.getPagesPerThread()));
                executor.setCorePoolSize(poolSize);
                executor.setMaximumPoolSize(poolSize);

                List<Future> futures = new ArrayList<>();
                for (URL url : urls) {

                    if (url == null) continue;

                    futures.add(executor.submit(new SiteMapSpiderRunnable(storageService, url, option, parser)));


                }
                List<Future> futuresCopy = new ArrayList<>(futures);
                int prevFuturesSize = futuresCopy.size();
                while (futuresCopy.size() > 0) {
                    ThreadUtils.sleepNoInterrupt(TimeUnit.SECONDS.toMillis(10));

                    futuresCopy = futures.stream().filter(f -> !(f.isDone() || f.isCancelled())).collect(Collectors.toList());

                    if (prevFuturesSize == futuresCopy.size()) {
                        ThreadUtils.sleepNoInterrupt(TimeUnit.MINUTES.toMillis(1));

                    }
                    futuresCopy = futures.stream().filter(f -> !(f.isDone() || f.isCancelled())).collect(Collectors.toList());
                    if (prevFuturesSize == futuresCopy.size()) {
                        futures.forEach(f -> f.cancel(true));
                    }
                    prevFuturesSize = futuresCopy.size();
                }
            } finally {
                try {
                    executor.shutdown();
                } catch (Exception e) {
                    logger.error(option.getSiteKey() + ". While shutting donw thread pool:", e);
                }
            }
        } catch (IOException e) {
            logger.error(e);
        }
        return new SiteMapSpiderResult(option.getSiteKey(), true);
    }
}


