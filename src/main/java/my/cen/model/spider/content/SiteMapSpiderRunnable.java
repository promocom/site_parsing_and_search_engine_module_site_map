package my.cen.model.spider.content;


import my.cen.model.spider.SiteParsingOptions;
import my.cen.model.spider.http.HttpResponse;
import my.cen.parser.IParser;
import my.cen.service.storage.IStorageService;
import my.cen.utils.DocumentUtils;

import static my.cen.utils.DocumentUtils.*;

import my.cen.utils.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by PC on 23.11.2014.
 */
public class SiteMapSpiderRunnable implements Runnable {
    private static final String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36";
    private static final int HTTP_RESP_200 = 200;
    private static final int HTTP_RESP_300 = 300;
    private static final int HTTP_RESP_400 = 400;
    private static final int HTTP_RESP_500 = 500;
    private static final int HTTP_RESP_404 = 404;
    private static final int HTTP_AWAIT_TIMEOUT = 500000; //millis


    private static final Logger logger = Logger.getLogger(SiteMapSpiderRunnable.class);

    private URL link;

    private IStorageService storageService;

    private final SiteParsingOptions options;
    private final IParser parser;

    public SiteMapSpiderRunnable(IStorageService storageService, URL link,
                                 SiteParsingOptions options, IParser parser) {
        super();
        this.link = link;

        this.options = options;
        this.storageService = storageService;
        this.parser = parser;
    }

    @Override
    public void run() {
        if (!doInitialCheck()) {
            if (!options.getSite().toString().equals(link.toString())) {
                return;
            }
        }
        HttpResponse resp = load(link);
        if (resp == null) {
            logger.info(String.format("Unable to process url %s", this.link.toString()));

            return;
        }
        if (resp.getRespCode() < HTTP_RESP_300) {
            process2XXRespCode(resp.getDocumet());
            return;
        } else {
            if (resp.getRespCode() < HTTP_RESP_400) {
                process3XXRespCode(resp);
                return;
            } else {
                processErrorRespCode(resp);
            }
        }
    }

    private void processErrorRespCode(HttpResponse resp) {
        if (resp.getRespCode() >= HTTP_RESP_500) {
             processError(false,"Http code "+resp.getRespCode());
            return;
        } else {
            processError(false, "Http code " + resp.getRespCode());//process 404 immediately
            return;
        }
    }

    private void process3XXRespCode(HttpResponse resp) {
        if (resp.getFollowedResponse() == null || resp.getFollowedResponse().getDocumet() == null) {
            setStatus(IStorageService.QueueItemStatus.EMPTY);
            return;
        }
        HttpResponse subresp = resp.getFollowedResponse();
        do {
            if (subresp.getFollowedResponse() != null) {

                subresp = subresp.getFollowedResponse();
            }
        } while (subresp != null && subresp.getFollowedResponse() != null);
        if (subresp != null) {
            String movedLocation = resp.getHeaders().get("Location");
            if (movedLocation != null) {
                URL movedUrl = normalizeLink(movedLocation, link.toString());
                if (movedUrl != null) {
                    if (movedUrl.getHost().equals(link.getHost())) {
                        storageService.index(movedUrl.toString());
                    } else {
                        storageService.indexExternalLink(movedUrl.toString());
                    }

                }
            }
        }
        setStatus(IStorageService.QueueItemStatus.REDIRECT);
    }

    private void process2XXRespCode(Document doc) {


        parseDocument(doc);

        try {

            Map<String, Object> map = new HashMap<>();
            map.put("url", link.toString());
            map.put("dom", doc);
            map.put("utils", new DocumentUtils());
            map.put("init", false);
            this.parser.parse(map);
            map.remove("dom");
            map.remove("utils");
            map.remove("init");
            if (!(boolean) map.get("skip")) {
                map.remove("skip");
                Calendar processed = Calendar.getInstance();
                // map.put("@processed", new Date());
                map.put("url", StringUtils.getRelativePart(link.toString()));
                String img = (String) map.get("img");
                if (img != null) {
                    try {
                        if (new URL(img).getHost().equals(link.getHost())) {
                            map.put("img", StringUtils.getRelativePart(img));
                        }
                    } catch (Exception e) {

                    }
                }
                SimpleDateFormat formated = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZ");
                if (map.get("date") != null) {
                    Calendar created = Calendar.getInstance();
                    created.setTime((Date) map.get("date"));
                    if (
                            processed.get(Calendar.YEAR) == created.get(Calendar.YEAR) &&
                                    processed.get(Calendar.MONTH) == created.get(Calendar.MONTH) &&
                                    processed.get(Calendar.DAY_OF_MONTH) == created.get(Calendar.DAY_OF_MONTH) &&
                                    created.get(Calendar.HOUR_OF_DAY) == 0 &&
                                    created.get(Calendar.MINUTE) == 0 &&
                                    created.get(Calendar.SECOND) == 0
                            ) {
                        map.put("date", formated.format(new Date()));
                    } else if (created.getTime().getTime() > System.currentTimeMillis()) {
                        map.put("date", formated.format(new Date()));
                    } else {

                        map.put("date", formated.format(created.getTime()));
                    }
                } else {
                    map.put("date", formated.format(new Date()));
                }
                map.put("doctype", StringUtils.getDocumentType(options.getSite()));
                storageService.persist(link.toString(), ((String) map.get("header")).trim(), map);
            } else {
                try {
                    storageService.setStatus(link.toString(), IStorageService.QueueItemStatus.EMPTY);
                } catch (Exception err) {
                    logger.error(String.format("Unable to set <PROCESSED> status for url %s", link), err);
                }
            }
        } catch (Exception e) {
            logger.error("While processing " + link, e);
        }
    }


    private boolean doInitialCheck() {
        for (String exclus : options.getExclusions()) {
            if (link.toString().toLowerCase().contains(exclus)) {
                setStatus(IStorageService.QueueItemStatus.COMPLETE);
                return false;
            }
        }
        try {
            new URI(link.getProtocol(), link.getUserInfo(), link.getHost(), link.getPort(), link.getPath(),
                    link.getQuery(), link.getRef());
        } catch (Exception err) {
            processError(false, err.getMessage());//process 404 immediately

            return false;
        }
        return true;
    }

    private void processError(boolean immediate, String reason) {
        storageService.processError(link.toString(), immediate, reason);
    }

    private void setStatus(IStorageService.QueueItemStatus status) {
        setStatus(link, status);
    }

    private void setStatus(URL url, IStorageService.QueueItemStatus status) {
        storageService.setStatus(url.toString(), status);
    }


    private void parseDocument(Document document) {
        Collection<String> urls = parseDocumentLinks(document, link.toString());
        Collection<String> domainDocuments = filterOutByDomain(new ArrayList<>(urls), link.getHost());

        Collection<String> nonDomainDocuments = new ArrayList<>(urls);
        nonDomainDocuments.removeAll(domainDocuments);
        domainDocuments.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                for (String exclusion : options.getExclusions()) {
                    if (s.contains(exclusion))
                        return false;
                }
                return true;
            }
        }).forEach(url -> storageService.index(url));
        nonDomainDocuments.stream().map(s -> {
            try {
                return new URL(s).getHost();
            } catch (MalformedURLException e) {
                return "";
            }
        }).forEach((url -> storageService.indexExternalLink(url)));


    }

    public HttpResponse load(URL url) {
        url = encodeURL(url);
        try {
            Connection.Response response = null;
            int statusCode = 0;
            try {
                if (url == null) throw new IOException();
                response = Jsoup.connect(url.toString())
                        .header("User-Agent", USER_AGENT_VALUE).followRedirects(false).timeout(HTTP_AWAIT_TIMEOUT).execute();
                statusCode = response.statusCode();
            } catch (HttpStatusException err) {
                statusCode = err.getStatusCode();
            } catch (ConnectException err) {
                statusCode = HTTP_RESP_404;
            } catch (UnknownHostException err) {
                statusCode = HTTP_RESP_404;
            } catch (IOException err) {
                statusCode = HTTP_RESP_500;
            } catch (Exception err) {
                statusCode = HTTP_RESP_500;
            }
            if (statusCode < HTTP_RESP_300) {
                return new HttpResponse(statusCode, response.headers(), response.parse());
            } else {
                if (statusCode < HTTP_RESP_400) {


                    if (response.header("Location") == null) {
                        return new HttpResponse(HTTP_RESP_404, null, null);
                    }
                    URL mainURL = normalizeLink(response.header("Location"), url.toString());
                    if (!mainURL.toString().replace(WORLD_WIDE_WEB, EMPTY_STRING).contains(url.getHost().replace(WORLD_WIDE_WEB, EMPTY_STRING))) {

                        processError(true, String.format("Skiping external redirect from %s to %s (%s !=%s)", link.toString(), response.header("Location"), response.header("Location").replace(WORLD_WIDE_WEB, EMPTY_STRING), url.getHost()));
                        return null;
                    }

                    if (mainURL == null) {
                        try {
                            return load(new URL(response.header("Location")));
                        } catch (Exception err) {
                            processError(true, err.getMessage());
                            return null;
                        }
                    }
                    if (mainURL.toString().equals(url.toString())) {
                        try {
                            response = Jsoup.connect(url.toString()).followRedirects(false).timeout(50000).execute();
                        } catch (Exception err) {
                            return null;
                        }
                        return new HttpResponse(HTTP_RESP_200, response.headers(), response.parse());
                    }


                    HttpResponse ret = load(mainURL);

                    return ret == null ? null :
                            new HttpResponse(ret, response.statusCode(), response.headers(), null);

                } else {
                    return new HttpResponse(statusCode, null, null);
                }
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
        return null;
    }


}
