package my.cen.model.spider;

import java.io.File;
import java.util.Collection;
import java.util.Set;

/**
 * Created  on 9/16/2015.
 */
public interface ISiteParsingOptionsHolder {
    SiteParsingOptions load(File file);

    SiteParsingOptions get(String site);


    Set<String> allSites();

    Collection<SiteParsingOptions> allOptions();
    Collection<SiteParsingOptions> importedOptions();

    int size();

    void addFromTemplate(String domain);

}
