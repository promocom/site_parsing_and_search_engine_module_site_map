package my.cen.discovery;

import my.cen.utils.ThreadUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created  on 9/15/2015.
 */
public class DiscoveryThread extends Thread {
    private static final Logger logger = Logger.getLogger(DiscoveryThread.class);
    private final String homeFolder;
    private final int discoveryThreadPoolSize;


    public DiscoveryThread(String homeFolder, int discoveryThreadPoolSize) {
        this.homeFolder = homeFolder;
        this.discoveryThreadPoolSize = discoveryThreadPoolSize;

    }

    @Override
    public void run() {

        while (true) {
            try {
                try (SiteDiscoveryServiceImpl discovery = new SiteDiscoveryServiceImpl(new File(homeFolder + "discovery"), discoveryThreadPoolSize)) {
                    List<String> files = Arrays.asList(new File(homeFolder).list()).stream().filter(s -> s.endsWith(".ext")).collect(Collectors.toList());

                    for (String file : files) {
                        String path = homeFolder + file;
                        discovery.processDomains(path, TimeUnit.HOURS.toMillis(5));
                    }
                    System.out.println("Discovery finished");

                }
                ThreadUtils.sleepNoInterrupt(TimeUnit.SECONDS.toMillis(10));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
