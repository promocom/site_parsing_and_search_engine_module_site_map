package my.cen.controllers;

import my.cen.model.spider.ISiteParsingOptionsHolder;
import my.cen.service.ElasticSearchService;
import my.cen.service.StatisticsService;
import my.cen.service.storage.impl.helpers.FileRemoveThread;
import my.cen.utils.DocumentUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.elasticsearch.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by PC on 12/28/2014.
 */
@Controller
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private ISiteParsingOptionsHolder optionsHolder;

    private long initialCount = 0;


    @RequestMapping(value = "/es/search", produces = "application/json;charset=utf-8")
    @ResponseBody
    public Object searchFor(@RequestParam("exp") String expression,
                            @RequestParam("by") String sortBy,
                            @RequestParam(value = "sid", required = false) String scrollId

    )

    {
        return elasticSearchService.searchFor(expression, 150, sortBy, scrollId,null);
    }

    @RequestMapping(value = "/es/exclude")
    @ResponseBody
    public void excludeSite(@RequestParam("site") String site) {
        elasticSearchService.excludeSite(site);
    }

    @RequestMapping(value = "/es/remove")
    @ResponseBody
    public void removeSite(@RequestParam("site") String site) {
        elasticSearchService.removeSite(site);
    }

    @RequestMapping(value = "/es/add")
    @ResponseBody
    public boolean addSite(@RequestParam("site") String site) {
        if (DocumentUtils.isParsableSite(site)) {
            optionsHolder.addFromTemplate(site);
            return true;
        }
        return false;
    }

    @Autowired
    private StatisticsService statisticsService;

    @RequestMapping(value = "/es/count")
    @ResponseBody
    public Object removeSite() {
        return statisticsService.getFullStatistics();
    }

    @RequestMapping(value = "/es/stats")
    @ResponseBody
    public Object stats(@RequestParam("cnt") int cnt) {
        return elasticSearchService.getStats(cnt);
    }

    @RequestMapping(value = "/es/cloud")
    @ResponseBody
    public Object tagsCloud(@RequestParam("cnt") int cnt, @RequestParam("tag") String tag) {
        return elasticSearchService.getTagCloud(tag, cnt);
    }

    @RequestMapping(value = "/es/img")
    @ResponseBody
    public void searchFor(@RequestParam("url") String url, HttpServletResponse resp) throws Exception {
        HttpResponse response = null;

        try {
            response = getContent(url);
        } catch (Exception e) {

        }
        if (response == null)
            response = getContent("http://lorempixel.com/100/100/abstract/grey/");
        HttpEntity entity = response.getEntity();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            resp.setHeader(header.getName(), header.getValue());
        }
        resp.getOutputStream().write(IOUtils.toByteArray(entity.getContent()));
        resp.getOutputStream().flush();

    }

    private HttpResponse getContent(String url) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        return client.execute(httpget);

    }
}

