package my.cen;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

/**
 * Created  on 10/21/2015.
 */
public class wiki {
    public static void main(String args[]) throws IOException {
        Iterator<String> lines = FileUtils.lineIterator(new File("/home/???/Загрузки/to_parse.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("/home/???/Загрузки/parsed.txt"));
        StringBuilder sb = null;
        double latitiude = 0;
        double longitude = 0;
        double latd = 0;
        double latm = 0;
        double lats = 0;

        char latNS = 0;
        double longd = 0;
        double longm = 0;
        double longs = 0;
        char longEW = 0;
        while (lines.hasNext()) {
            String line = lines.next().trim();
            if (line.startsWith("<title>")) {

                if (sb != null) {
                    if(!sb.toString().startsWith("File")&&!sb.toString().startsWith("Draft")&&!sb.toString().startsWith("Category")&&!sb.toString().startsWith("Template")&&!sb.toString().startsWith("Wikipedia"))
                    System.out.println(sb);
                    writer.write(sb.toString());
                    writer.newLine();

                }
                sb = new StringBuilder();
                latitiude = Double.NEGATIVE_INFINITY;
                longitude = Double.NEGATIVE_INFINITY;
                latd = Double.NEGATIVE_INFINITY;
                latm = Double.NEGATIVE_INFINITY;
                lats = Double.NEGATIVE_INFINITY;
                longd = Double.NEGATIVE_INFINITY;
                longm = Double.NEGATIVE_INFINITY;
                longs = Double.NEGATIVE_INFINITY;
                longEW = 0;
                latNS = 0;
                sb.append(line.replace("<title>", "").replace("</title>", "") + ";");
            } else if (line.startsWith("<redirect")) {
                sb.append(line.replace("<redirect title=\"", "").replace("\" />", "") + "|");
            } else {
                String parts[] = line.split("[|.*{{.*}}]");
                for (String part : parts) {
                    part = part.trim();
                    part = part.replace("&quot;", "''");
                    part = part.replace("?", "");
                    part = part.replace("′", "'");
                    part = part.replace("°", ".");
                    part = part.replace("º", ".");
                    part = part.replace("- ", "-");
                    part = part.replace("±½", "");
                    part = part.replace("'", "' ");
                    part = part.replace("' '", "''");
                    part = part.replace("  ", " ");
                    part = part.replace("&lt;", "<");
                    part = part.replace("&gt;", ">");
                    part = part.replace("<!--", "");
                    part = part.replace("-->", "");
                    part = part.replace("degrees", "");
                    part = part.replace("minutes", "");
                    part = part.replace("seconds", "");
                    String keyvalue[] = part.split("=");


                    if (keyvalue.length == 2) {
                        try {
                            switch (keyvalue[0].trim()) {
                                default:
                                    System.out.println(keyvalue[0] + " " + keyvalue[1]);
                                    break;
                                case "longitude":
                                    try {
                                        longitude = Double.parseDouble(keyvalue[1].trim());
                                    } catch (Exception e) {
                                        String coords[] = keyvalue[1].trim().split(" ");
                                        switch (coords.length) {
                                            case 1:
                                                break;
                                            case 2:try{
                                                longd = Double.parseDouble(coords[0].trim().split("[.]")[0]);
                                                longm = Double.parseDouble(coords[0].trim().split("[.]")[1].replace(".", "").replace("'", ""));
                                                longEW = coords[1].charAt(0);}catch (Exception ex){}
                                                break;
                                            default: {try{
                                                if (coords[0].endsWith(".")) {
                                                    longd = Double.parseDouble(coords[0].replace(".", ""));
                                                }
                                                if (coords[1].endsWith("'")) {
                                                    longm = Double.parseDouble(coords[1].replace("'", ""));
                                                }
                                                if (coords[2].endsWith("''")) {
                                                    longs = Double.parseDouble(coords[2].replace("''", ""));
                                                    if (coords.length == 3) {
                                                        longEW = 'W';
                                                    } else {

                                                        longEW = coords[3].charAt(0);
                                                    }
                                                } else {
                                                    longEW = coords[2].charAt(0);
                                                }}catch (Exception ex){}
                                            }
                                        }
                                    }
                                    break;
                                case "latitude":
                                    if (keyvalue[1].trim().startsWith("{{")) {
                                        String[] latdecDeg = keyvalue[1].replace("}}", "").split("[|]");
                                        latd = Double.parseDouble(latdecDeg[1]);
                                        latm = Double.parseDouble(latdecDeg[2]);
                                        lats = Double.parseDouble(latdecDeg[3]);
                                        latNS = latdecDeg[4].charAt(0);
                                        break;
                                    }
                                    try {
                                        latitiude = Double.parseDouble(keyvalue[1].trim());
                                    } catch (Exception e) {
                                        String coords[] = keyvalue[1].trim().split(" ");
                                        switch (coords.length) {
                                            case 1:
                                                break;
                                            case 2:
                                                try {
                                                    // coords[0]=coords[0].trim().replace("°","");
                                                    latd = Double.parseDouble(coords[0].trim().split("[.]")[0]);
                                                    latm = Double.parseDouble(coords[0].trim().split("[.]")[1].replace(".", "").replace("'", ""));
                                                    if (coords.length > 1)
                                                        latNS = coords[1].charAt(0);
                                                } catch (Exception ex) {
                                                    System.err.println(line);
                                                }
                                                break;
                                            default: {
try{
                                                if (coords[0].endsWith(".")) {
                                                    latd = Double.parseDouble(coords[0].replace(".", ""));
                                                }
                                                if (coords[1].endsWith("'")) {
                                                    latm = Double.parseDouble(coords[1].replace("'", ""));
                                                }
                                                if (coords[2].endsWith("''")) {
                                                    lats = Double.parseDouble(coords[2].replace("''", ""));
                                                    if (coords.length == 3) {
                                                        latNS = 'N';
                                                    } else {
                                                        latNS = coords[3].charAt(0);

                                                    }
                                                } else {
                                                    latNS = coords[2].charAt(0);
                                                }
                                            }catch (Exception ex){}
                                        }}
                                    }
                                    break;
                                case "latd":
                                    if (keyvalue[1].trim().length() > 0)
                                        try {

                                            latd = Double.parseDouble(keyvalue[1].trim());
                                        } catch (Exception e) {
                                        }
                                    break;
                                case "latm":
                                    if (keyvalue[1].trim().length() > 0)try{
                                        latm = Double.parseDouble(keyvalue[1].trim());}catch (Exception e){}
                                    break;
                                case "lats":
                                    if (keyvalue[1].trim().length() > 0)
                                        try {
                                            latm = Double.parseDouble(keyvalue[1].trim());
                                        } catch (Exception e) {

                                        }
                                    break;
                                case "latNS":
                                    if (keyvalue[1].trim().length() > 0)try{
                                        latNS = keyvalue[1].trim().charAt(0);}catch (Exception e){}
                                    break;
                                case "longEW":
                                    if (keyvalue[1].trim().length() > 0)try{
                                        longEW = keyvalue[1].trim().charAt(0);}catch (Exception e){}
                                    break;
                                case "longd":
                                    if (keyvalue[1].trim().length() > 0)
                                        try {
                                            longd = Double.parseDouble(keyvalue[1].trim());
                                        } catch (Exception e) {
                                        }
                                    break;
                                case "longm":
                                    if (keyvalue[1].trim().length() > 0)try{
                                        longm = Double.parseDouble(keyvalue[1].trim());}catch (Exception e){}
                                    break;
                                case "longs":
                                    if (keyvalue[1].trim().length() > 0)try{
                                        longm = Double.parseDouble(keyvalue[1].trim());}catch (Exception e){}
                                    break;
                                case "latmin":
                                case "latmax":
                                    break;
                            }
                        } catch (Exception e) {
                            System.err.print(line);
                            e.printStackTrace();
                            System.exit(-10);
                        }
                    } else {
                        //System.out.println(part);
                    }
                }
            }


        }
        writer.flush();
        writer.close();
    }
}
