package my.cen.service.impl;

import my.cen.service.SiteState;
import my.cen.service.StatisticsService;
import org.eclipse.jetty.util.ArrayQueue;
import org.elasticsearch.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

/**
 * Created  on 9/19/2015.
 */
public class StatisticsServiceImpl implements StatisticsService {
    private final ArrayQueue<Long> totalProcessedStack;
    private final ArrayQueue<Long> totalremovedStack;
    private final Map<String, SiteState> siteStates;
    private final Map<String, Queue<Integer>> siteIndiciesSize;
    private final int stackSize;

    public StatisticsServiceImpl(int stackSize) {
        this.stackSize = stackSize;
        totalProcessedStack = new ArrayQueue<>(stackSize, 1);
        totalremovedStack = new ArrayQueue<>(stackSize, 1);
        siteIndiciesSize = new HashMap<>();
        siteStates = new HashMap<>();
    }

    @Override
    public void setTotalProcessed(long count) {
        totalProcessedStack.offer(count);
        if (totalProcessedStack.size() >= stackSize) {
            totalProcessedStack.poll();
        }
    }

    @Override
    public void setTotalRemoved(long count) {
        totalremovedStack.offer(count);
        if (totalremovedStack.size() >= stackSize) {
            totalremovedStack.poll();
        }
    }

    @Override
    public void setSiteState(String site, SiteState state) {
        siteStates.put(site, state);
    }

    @Override
    public void setSiteIndexCount(String site, int indexCount) {
        Queue<Integer> counts = siteIndiciesSize.get(site);
        if (counts == null) {
            counts = new ArrayQueue<>(stackSize, 1);
            siteIndiciesSize.put(site, counts);
        }
        counts.offer(indexCount);
        if (counts.size() >= stackSize) {
            counts.poll();
        }
    }

    @Override
    public Map<String, Object> getFullStatistics() {
        return new ImmutableMap.Builder<String, Object>()
                .put("processed", totalProcessedStack)
                .put("removed", totalremovedStack)
                .put("state", siteStates)
                .put("remaining", siteIndiciesSize)
                .build();
    }
}
