package my.cen.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import my.cen.model.spider.SiteParsingOptionsHolder;
import my.cen.service.ElasticSearchService;
import my.cen.utils.StorageUtils;
import my.cen.utils.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.index.Term;
import org.elasticsearch.action.admin.indices.alias.Alias;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.optimize.OptimizeRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.support.QuerySourceBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.collect.ImmutableMap;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregator;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCount;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by PC on 15.12.2014.
 */

public class ElasticSearchServiceImpl implements ElasticSearchService {
    private final static Logger logger = Logger.getLogger(ElasticSearchService.class);


    private final Client client;

    public ElasticSearchServiceImpl(Client client) {
        this.client = client;
    }

    @Override
    public Map searchFor(String expression, int maxCount, String sortField, String scrollId, Date startDate) {

        SearchResponse scrollResp = null;

        expression = expression.replace("header:", "");

        try {


            if (scrollId == null) {
                BoolQueryBuilder q = QueryBuilders.boolQuery();
                if ("*".equals(expression)) {
                    q.must(QueryBuilders.matchAllQuery());
                } else {
                    expression = expression.replaceAll("[^a-zA-Zа-яА-Я0-9_ ]", " and ");
                    String[] words = expression.toLowerCase().split(" and ");

                    for (String word : words) {
                        try {
                            client.prepareIndex("searches", "search", word)
                                    .setSource(new ObjectMapper().writeValueAsString(ImmutableMap.of("word", word))).execute()
                                    .actionGet();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (!"".equals(word))
                            q.must(QueryBuilders.boolQuery().should(QueryBuilders.termQuery("header", word.trim())).should(QueryBuilders.termQuery("tags", word.trim())));
                    }
                }
                for (String exclude : excludeSites) {
                    q.mustNot(QueryBuilders.termQuery("doctype", exclude));
                }
                SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                if (startDate != null) {
                    Calendar start = Calendar.getInstance();
                    Calendar end= Calendar.getInstance();
                    start.setTime(startDate);
                    end.setTime(startDate);
                    start.set(Calendar.HOUR,0);
                    start.set(Calendar.MINUTE,0);
                    start.set(Calendar.SECOND, 0);
                    start.set(Calendar.MILLISECOND, 0);
                    end.set(Calendar.HOUR, 23);
                    end.set(Calendar.MINUTE, 59);
                    end.set(Calendar.SECOND, 59);
                    end.set(Calendar.MILLISECOND,999);
                    q.must(QueryBuilders.rangeQuery("date").from(sourceFormat.format(start.getTime())).to(sourceFormat.format(end.getTime())));
                }

                scrollResp = client.prepareSearch(new String[]{})
                        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                        .setQuery(q).setSize(maxCount)
                        .setScroll(new TimeValue(60000)).addSort(sortField, SortOrder.DESC)
                        .addFields("header", "img", "date", "url", "tags", "doctype", "geo.point")
                        .addAggregation(AggregationBuilders.terms("tags").field("tags").size(100).order(Terms.Order.count(false)))
                        .execute()
                        .actionGet();
            } else {
                scrollResp = client.prepareSearchScroll(scrollId).setScroll(new TimeValue(600000)).execute().actionGet();

            }
        } catch (Exception err) {
            logger.error(err, err);
        }
        SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        Map result = new HashMap<>();
        if (scrollResp != null && scrollResp.getHits() != null && scrollResp.getHits().hits().length > 0) {
            List data = new ArrayList<>();
            List<String> tags = new ArrayList<>();
            if (scrollId == null) {

                Terms terms = scrollResp.getAggregations().get("tags");
                for (Terms.Bucket term : terms.getBuckets()) {
                    tags.add(term.getKey());
                }
            }
            result.put("tags", tags);
            result.put("sid", scrollResp.getScrollId());
            result.put("data", data);
            result.put("total", scrollResp.getHits().getTotalHits());
            for (SearchHit hit : scrollResp.getHits().hits()) {
                HashMap subresult = new HashMap();

                subresult.put("header", hit.getFields().get("header").value());
                try {
                    subresult.put("tags", hit.getFields().get("tags").value());
                } catch (Exception e) {

                }
                try {
                    subresult.put("geo", hit.getFields().get("geo.point").value());
                } catch (Exception e) {

                }
                try {
                    subresult.put("date", targetFormat.format(sourceFormat.parse(hit.getFields().get("date").value())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String url = hit.getFields().get("url").value();
                if (!url.startsWith("http")) {
                    url = "http://" + (hit.getFields().get("doctype").value()) + url;
                }
                subresult.put("url", url);
                String img = hit.getFields().get("img").value();
                if (!img.startsWith("http")) {
                    img = "http://" + (hit.getFields().get("doctype").value()) + img;
                }
                subresult.put("img", img);
                data.add(subresult);

            }
            return result;
        }

        return result;
    }


    public void index(String indexName, String typeName, String id, Object object) {
        try {
            client.prepareIndex(indexName, typeName, id)
                    .setSource(new ObjectMapper().writeValueAsString(object)).execute()
                    .actionGet();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void processQueryResults(String[] indicies, QueryBuilder query, ResultProcessor resultProcessor, String... fields) {
        processQueryResults(indicies, query,null, resultProcessor, fields);
    }

    public void processQueryResults(String[] indicies, QueryBuilder query, FilterBuilder filter, ResultProcessor resultProcessor, String... fields) {
        SearchResponse scrollResp = null;
        try {

            SearchRequestBuilder builder = client.prepareSearch(indicies)
                    .setPostFilter(FilterBuilders.missingFilter("geo.point"))
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)


                    .addFields(fields).setExplain(true)
                    .setScroll(new TimeValue(600000));
            if (query != null) {
                builder.setQuery(query);
            }
            if (filter != null) {
                builder.setPostFilter(filter);
            }
            scrollResp = builder.execute().actionGet();
        } catch (Exception err) {
        }
        int currentCount = 0;
        if (scrollResp != null && scrollResp.getHits() != null) {
            final long totalcount = scrollResp.getHits().getTotalHits();
            while (scrollResp != null) {
                for (SearchHit hit : scrollResp.getHits().hits()) {
                    final int current = ++currentCount;
                    resultProcessor.process(hit, totalcount, current);
                }
                scrollResp = client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(600000)).execute().actionGet();
                if (scrollResp.getHits().getHits().length == 0) {
                    break;
                }
            }
        }

    }


    public void optimize() {
        try {
            if (!client.admin().indices().prepareExists("documents").execute().actionGet().isExists()) {
                client.admin().indices().prepareCreate("documents")
                        .addMapping("site", ImmutableMap.of(
                                "_source", ImmutableMap.of("enabled", false),
                                "properties", new ImmutableMap.Builder<>()
                                        .put("img", ImmutableMap.of("type", "string", "index", "no", "store", "yes"))
                                        .put("url", ImmutableMap.of("type", "string", "index", "no", "store", "yes"))
                                        .put("header", ImmutableMap.of("type", "string", "index", "analyzed", "store", "yes"))
                                        .put("tags", ImmutableMap.of("type", "string", "index", "analyzed", "store", "yes"))
                                        .put("doctype", ImmutableMap.of("type", "string", "index", "not_analyzed", "store", "yes"))
                                        .put("date", ImmutableMap.of("type", "date", "index", "analyzed", "store", "yes"))
                                        .put("geo.point", ImmutableMap.of("type", "geo_point", "index", "no", "store", "yes"))
                                        .build()
                        ))


                        .execute().actionGet();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        client.admin().indices().optimize(new OptimizeRequest().maxNumSegments(1)).actionGet();
    }

    private static final List<String> excludeSites = new ArrayList<>();

    @Override
    public void removeSite(String site) {
        client.prepareDeleteByQuery().setQuery(QueryBuilders.termQuery("doctype", site)).execute().actionGet();
    }

    @Override
    public void excludeSite(String site) {
        try {
            excludeSites.add(StringUtils.getDocumentType(new URL(site)));
        } catch (Exception e) {
            //skip
        }
    }


    @Override
    public Map<String, Long> getStats(int count) {
        SearchResponse aggregationResp = client.prepareSearch(new String[]{})
                .setSize(0)
                .addAggregation(AggregationBuilders.terms("sites").field("doctype").order(Terms.Order.count(false)).size(count))
                .setQuery(QueryBuilders.matchAllQuery()).execute().actionGet();
        Terms counts = aggregationResp.getAggregations().get("sites");
        Map<String, Long> res = new LinkedHashMap<>();
        List<Terms.Bucket> buckets = counts.getBuckets();
        for (Terms.Bucket b : buckets) {
            res.put(b.getKey(), b.getDocCount());
        }
        return res;
    }

    @Override
    public Map<String, Long> getTagCloud(String tag, int count) {
        SearchResponse aggregationResp = client.prepareSearch(new String[]{})
                .setSize(0)
                .addAggregation(AggregationBuilders.terms("tags").field("tags").order(Terms.Order.count(false)).size(count))
                .setQuery(QueryBuilders.fuzzyQuery("tags", tag)).execute().actionGet();
        Terms counts = aggregationResp.getAggregations().get("tags");
        Map<String, Long> res = new LinkedHashMap<>();
        List<Terms.Bucket> buckets = counts.getBuckets();
        for (Terms.Bucket b : buckets) {
            res.put(b.getKey(), b.getDocCount());
        }
        return res;
    }



    @Override
    public long count() {
        return client.prepareCount().setQuery(QueryBuilders.matchAllQuery()).execute().actionGet().getCount();
    }
}
