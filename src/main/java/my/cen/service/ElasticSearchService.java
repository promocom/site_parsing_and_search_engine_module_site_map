package my.cen.service;

import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Created by PC on 15.12.2014.
 */
public interface ElasticSearchService {


    void processQueryResults(String[] indicies, QueryBuilder builder, ResultProcessor resultProcessor, String... fields);

    void processQueryResults(String[] indicies, QueryBuilder query, FilterBuilder filter, ResultProcessor resultProcessor, String... fields);

    void index(String indexName, String typeName, String кid, Object object);


    Map searchFor(String expression, int maxCount, String sortField, String scrollId, Date startDate);


    void optimize();

    void removeSite(String site);

    public void excludeSite(String site);

    interface ResultProcessor {
        void process(SearchHit hit, long totalCount, int currentNumber);
    }


    Map<String, Long> getStats(int count);

    Map<String, Long> getTagCloud(String tag, int count);

    long count();

}
