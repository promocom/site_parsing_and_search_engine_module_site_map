package my.cen.service.storage;

import java.net.URL;
import java.util.List;

/**
 * Created by PC on 12/19/2014.
 */
public interface IStorageService {
    enum QueueItemStatus {
        ADDED, EMPTY, ERROR, REDIRECT, LANGUAGE, COMPLETE, SKIP
    }

    void index( String url);

    void indexExternalLink( String host);


    void setStatus(String url, QueueItemStatus status);

    void processError( String url, boolean immediate, String reason);

    List<URL> list( int count);

    int tempIndexCount();

    void persist( String url, String id, Object payload);


}
