package my.cen.service.storage.impl.helpers;

import my.cen.utils.ThreadUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This thread is responsible for deletion of indexed files
 * Created  on 9/8/2015.
 */
public class FileRemoveThread extends Thread {
    private static final Logger logger = Logger.getLogger(FileRemoveThread.class);
    private static final ConcurrentLinkedQueue<File> queue = new ConcurrentLinkedQueue<>();
    public static final AtomicLong deleted = new AtomicLong();


    @Override
    public void run() {
        while (true) {
            try {
                File toRemove = queue.poll();
                if (toRemove != null) {
                    removeFile(toRemove);
                    deleted.incrementAndGet();
                } else {
                    ThreadUtils.sleepNoInterrupt(1000);
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }


    }

    public static void enqueueFile(File file) {
        while (!queue.offer(file)) {
            ThreadUtils.sleepNoInterrupt(100);
        }

    }

    private void removeFile(File fl) {
        while (fl.exists()) {

            try {
                Files.delete(fl.toPath());
            } catch (IOException e) {

            }
        }
    }
}
