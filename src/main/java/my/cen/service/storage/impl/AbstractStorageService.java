package my.cen.service.storage.impl;

import my.cen.service.ElasticSearchService;
import my.cen.service.storage.IStorageService;
import my.cen.utils.StringUtils;

import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by PC on 12/19/2014.
 */
public abstract class AbstractStorageService implements IStorageService {


    protected ElasticSearchService elasticSearchService;
    protected String site;




    public AbstractStorageService(ElasticSearchService elasticSearchService,String site) {
        this.elasticSearchService = elasticSearchService;
        this.site = site;
    }


    public abstract <T> URL decode(T object);

    public final void persist(String url, String id, Object payload) {
        elasticSearchService.index("documents", "site", site + ":" + StringUtils.encode(id), payload);
        setStatus( url.toString(), IStorageService.QueueItemStatus.COMPLETE);

    }


}
