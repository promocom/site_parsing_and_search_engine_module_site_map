package my.cen.service.storage.impl;

import my.cen.index.HashStringIndex;
import my.cen.index.StringIndex;
import my.cen.service.ElasticSearchService;
import my.cen.service.storage.impl.helpers.FileRemoveThread;
import my.cen.utils.DocumentUtils;
import my.cen.utils.StringUtils;
import my.cen.utils.ThreadUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.log4j.Logger;
import org.elasticsearch.common.base.Throwables;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import java.util.stream.Collectors;

/**
 * Created  on 8/28/2015.
 */
public class IndexBasedStorageService extends AbstractStorageService implements Closeable {
    private static final Logger logger = Logger.getLogger(IndexBasedStorageService.class);
    private final String storageFolder;
    private final String indexFolder;


    private final StringIndex stringIndex;
    private final StringIndex externalIndex;


    public IndexBasedStorageService(ElasticSearchService elasticSearchService, String storageFolder, String site, boolean hash) {

        super(elasticSearchService, site);
        if (!storageFolder.endsWith("/") && !storageFolder.endsWith("\\")) {
            this.storageFolder = storageFolder + "/";
        } else {
            this.storageFolder = storageFolder;
        }
        File ind = new File(storageFolder + site + ".ind");
        File ext = new File(storageFolder + site + ".ext");
        boolean exists = ind.exists();
        if (!exists) {
            try {
                ind.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!ext.exists()) {
            try {
                ext.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        externalIndex = new StringIndex(ext);
        if (hash)
            stringIndex = new HashStringIndex(ind);
        else
            stringIndex = new StringIndex(ind);
        try {
            externalIndex.open();
            stringIndex.open();
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
        if (!exists) {


            try {
                ind.createNewFile();

                // stringIndex.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        this.indexFolder = this.storageFolder;

        mkdirs(this.indexFolder);
        mkdirs(this.storageFolder);
    }

    @Override
    public void index(String url) {
        if (stringIndex.searchOrIndex(StringUtils.getRelativePart(url))) return;

        String filename = StringUtils.encode(url.toString());
        try {
            String storePath = getNewPath();
            mkdirs(storePath);
            FileUtils.write(new File(storePath + filename), url.toString());
        } catch (IOException e) {
        }
    }

    @Override
    public void indexExternalLink(String host) {
        externalIndex.searchOrIndex(host);
    }


    @Override
    public void setStatus(String url, QueueItemStatus status) {
        if (status != QueueItemStatus.SKIP) {
            // logger.info("Setting status " + status + " for url:" + url);
        }
        String filename = StringUtils.encode(url);
        removeFile(new File(getNewPath() + filename));
    }


    @Override
    public void processError(String url, boolean immediate, String reason) {
        logger.info("Processing error for url(" + reason + "):" + url);
        String filename = StringUtils.encode(url);

        File file = new File(getNewPath() + filename);
        int count = 0;
        try {
            List<String> lines = FileUtils.readLines(file);
            if (lines.size() > 1)
                count = Integer.parseInt(lines.get(1));

        } catch (Exception e) {

        }

        if (immediate || count + 1 > 10) {


            removeFile(new File(getNewPath() + filename));
        } else {
            try {
                FileUtils.write(new File(getNewPath() + filename), url + "\n" + String.valueOf(count + 1));
            } catch (Exception e) {

            }
        }
    }


    private void removeFile(File fl) {
        FileRemoveThread.enqueueFile(fl);
    }


    @Override
    public List<URL> list(int count) {

        mkdirs(getNewPath());

        return DocumentUtils.fileList(new File(getNewPath()).getAbsolutePath(), count).stream().map(file -> decode(new File(file))).collect(Collectors.toList());
    }

    @Override
    public int tempIndexCount() {
        return DocumentUtils.fileCount(new File(getNewPath()).getAbsolutePath());
    }

    @Override
    public <T> URL decode(T object) {
        File file = (File) object;
        try {
            return new URL(FileUtils.readLines(file).get(0));
        } catch (Exception e) {
            file.delete();
            return null;
        }
    }


    private void mkdirs(String path) {
        boolean created;
        do {
            File dirs = new File(path);
            created = dirs.exists() || dirs.mkdirs();
            if (!created) {
                logger.debug("Unable to create dirs for " + path);
                ThreadUtils.sleepNoInterrupt(10000);

            }
        } while (!created);
    }

    private String getNewPath() {
        return indexFolder + "/index/" + site + "/";
    }

    @Override
    public void close() throws IOException {

        try {
            stringIndex.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            stringIndex.close();
            try {
                externalIndex.flush();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                externalIndex.close();
            }
        }

    }
}
