package my.cen.service.storage.impl.helpers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created  on 8/28/2015.
 */
public class CasheHelper {
   public Map<String, AtomicInteger> cashe = new ConcurrentHashMap();

    public void cashe(String object) {
        cashe.put(object, new AtomicInteger(1));
    }

    public boolean isCashed(String object) {
        AtomicInteger counter = cashe.get(object);
        boolean exists = counter != null;
        if (exists) {
            counter.incrementAndGet();
        }
        return exists;
    }

    public Set invalidateLessAccessed(int threshold) {
        TreeSet<String> set = new TreeSet();
        if (cashe.size() < 10000) return set;
        for (Map.Entry<String, AtomicInteger> entry : new HashSet<>(cashe.entrySet())) {
            String key = entry.getKey();
            if (entry.getValue().get() < threshold) {
                set.add(key);
                cashe.remove(key);
            }
        }
        return set;

    }

    public void invalidate(String url) {
        cashe.remove(url);
    }

    public boolean isEmpty() {
        return cashe.size() == 0;
    }

    public boolean store(String path) {
        try (ObjectOutputStream fos = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(path)))) {

            fos.writeObject(cashe.entrySet()
                            .stream()
                            .collect(Collectors.toMap(Map.Entry::getKey,
                                    e -> e.getValue().get()))
            );
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean load(String path) {
        try (ObjectInputStream ois = new ObjectInputStream(new GZIPInputStream(new FileInputStream(path)))) {
            cashe.putAll(((Map<String, Integer>) ois.readObject())
                    .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                            e -> new AtomicInteger(e.getValue()))));


        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
