package my.cen.service;

import java.util.Map;
import java.util.Set;

/**
 * Created  on 9/19/2015.
 */
public interface StatisticsService {
    void setTotalProcessed(long count);

    void setTotalRemoved(long count);

    void setSiteState(String site, SiteState state);

    void setSiteIndexCount(String site, int indexCount);

    Map<String, Object> getFullStatistics();
}
