package my.cen.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created  on 2/7/16.
 */
public final class TimeUtils {
    private TimeUtils() {
    }

    public boolean isTimeDifferenceLessThanWeek(long time) {
        return System.currentTimeMillis() - time > TimeUnit.DAYS.toMillis(7);
    }

    public String getTimeShift(long time) {
        if (isTimeDifferenceLessThanWeek(time)) {
            long shift = System.currentTimeMillis() - time;
            long days = shift / TimeUnit.DAYS.toMillis(1);
            if (days > 0) return days + " days ago";
            long hours = shift / TimeUnit.HOURS.toMillis(1);
            if (hours > 0) return hours + " hours ago";
            long minutes = shift / TimeUnit.MINUTES.toMillis(1);
            if (minutes > 0) return minutes + " minutes ago";
            return "just now";
        }
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(time));
    }
}
