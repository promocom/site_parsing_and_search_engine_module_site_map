package my.cen.utils;

import org.elasticsearch.common.base.Throwables;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

/**
 * Created  on 8/29/2015.
 */
public final class StorageUtils {
    private StorageUtils() {
    }

    public static void storeSet(Set<String> set, File file) {
        try (FileWriter writer = new FileWriter(file)) {
            writer.write("\n"+set.toString().replace(",", "\n").replace("[", "").replace("]", "").replace(" ", ""));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }
}
