package my.cen.utils;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import org.apache.camel.converter.TimePatternConverter;
import org.apache.commons.lang.*;
import org.elasticsearch.common.collect.*;
import org.elasticsearch.common.joda.time.format.ISODateTimeFormat;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.format.datetime.DateFormatter;

import java.io.IOException;
import java.net.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created  on 9/6/2015.
 */
public final class DocumentUtils {
    public static final String HREF_ATTR = "href";
    public static final String EMPTY_STRING = "";
    public static final String HASH_STRING = "#";
    public static final String HREF_SELECTOR = "a[" + HREF_ATTR + "]";
    public static final String SLASH = "/";
    public static final String DOUBLE_SLASH = SLASH + SLASH;
    public static final String HTTP_PROTOCOL = "http";
    public static final String HTTPS_PROTOCOL = "https";
    public static final String COLON = ":";
    public static final String PREV_SIGN = "../";
    public static final String QUESTION_MARK = "?";
    public static final String WORLD_WIDE_WEB = "www.";

    public static final String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36";
    public static final String USER_AGENT_HEADER = "User-Agent";
    public static final String HTTP_PREFIX = HTTP_PROTOCOL + COLON + DOUBLE_SLASH;
    public static final String HTTPS_PREFIX = HTTPS_PROTOCOL + COLON + DOUBLE_SLASH;
    public static final String OG_TITLE = "meta[property=\"og:title\"]";
    public static final String OG_TYPE = "meta[property=\"og:type\"]";
    public static final String OG_IMAGE = "meta[property=\"og:image\"]";
    public static final String PUBLISHED_TIME = "meta[property=article:published_time]";
    public static final String MODIFIED_TIME = "meta[property=article:modified_time]";
    public static final String CONTENT = "content";
    private static final int HTTP_TIMEOUT = (int) TimeUnit.MINUTES.toMillis(2);

    public static String getAttrValue(Document doc, String selector, String attr) {
        return doc.select(selector).attr(attr);
    }

    public static Collection getAttrValueList(Document doc, String selector, String attr) {
        Set<String> set = new TreeSet<>();
        doc.select(selector).stream().map(e -> e.attr(attr)).collect(Collectors.toList()).forEach(e -> set.addAll(Arrays.asList(e.split(","))));
        return set;
    }

    protected Set<String> bodyContentList(Document doc, String selector) {
        return bodyContentList(doc, selector, ImmutableMap.of());
    }

    protected String dateModify(String date, SimpleDateFormat format) {
        date = date.replace(" в ", " ").replace("  ", " ").trim();
        if (date.contains("вчера")) {
            date = date.replace("вчера", format.format(new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1))));
        }
        if (date.contains("сегодня")) {
            date = date.replace("сегодня", format.format(new Date()));
        }
        return date;
    }


    public static Set<String> prepareKeywords(Collection<String> keywords) {
        Set<String> result = new TreeSet<>();
        keywords.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                s = s.trim();
                while (s.contains("  ")) {
                    s = s.replace("  ", " ");
                }
                if (s.length() > 1) {
                    result.add(prepareTag(s));
                }
            }
        });
        return result;
    }

    public static String prepareTag(String text) {
        return org.apache.commons.lang.StringUtils.join(new TreeSet<>(Arrays.asList(text.replace("-", "").toLowerCase().split(" "))), "_");
    }

    public Set<String> bodyContentList(Document doc, String selector, Map<String, String> replacers) {
        Set<String> result = Sets.newTreeSet();
        for (Element el : doc.select(selector).select("*")) {
            for (TextNode node : el.textNodes()) {
                String text = node.text().trim();
                for (Map.Entry<String, String> replacer : replacers.entrySet()) {
                    text = text.trim().toLowerCase().replace(replacer.getKey().toLowerCase(), replacer.getValue()).trim();
                }
                if (text.trim().length() > 0) {
                    if (!"".equals(text.trim()))
                        result.add(text.toLowerCase().trim());

                }

            }

        }
        return result;
    }

    public List<String> trimTags(List<String> list) {
        return list.stream().map(String::trim).collect(Collectors.toList());
    }

    public static boolean isParsableSite(String domain) {
        try {
            String site = HTTP_PREFIX + domain;
            Connection.Response rep = Jsoup.connect(site)
                    .header(USER_AGENT_HEADER, USER_AGENT_VALUE).followRedirects(false)
                    .timeout(HTTP_TIMEOUT)
                    .execute();

            Document doc = rep.parse();
            String mainPageTitle = getAttrValue(doc, OG_TITLE, CONTENT).trim();
            if (!"".equals(mainPageTitle)) {
                Collection<String> urls = parseDocumentLinks(doc, site);
                urls = filterOutByDomain(urls, domain, false);
                for (String subUrl : urls) {
                    rep = Jsoup.connect(subUrl)

                            .header(USER_AGENT_HEADER, USER_AGENT_VALUE).followRedirects(false).timeout(HTTP_TIMEOUT).execute();
                    doc = rep.parse();
                    if ("article".equals(getAttrValue(doc, OG_TYPE, CONTENT).trim())) {
                        String published = getAttrValue(doc, PUBLISHED_TIME, CONTENT);
                        String modified = getAttrValue(doc, MODIFIED_TIME, CONTENT);
                        String title = getAttrValue(doc, OG_TITLE, CONTENT).trim();
                        String image = getAttrValue(doc, OG_IMAGE, CONTENT).trim();
                        String kw1 = getAttrValue(doc, "meta[name=\"news_keywords\"]", CONTENT).trim();
                        String kw2 = getAttrValue(doc, "meta[name=\"article:tag\"]", CONTENT).trim();
                        String kw3 = getAttrValue(doc, "meta[name=\"keywords\"]", CONTENT).trim();

                        String time = published.equals(EMPTY_STRING) ? modified : published;
                        if (time.length() > 23 && !title.equals(EMPTY_STRING) && !image.equals(EMPTY_STRING)
                                && (!EMPTY_STRING.equals(kw1) || !EMPTY_STRING.equals(kw2) || !EMPTY_STRING.equals(kw3))
                                ) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return false;
    }

    public String bodyContent(Document doc, String selector) {
        StringBuilder builder = new StringBuilder();
        for (Element el : doc.select(selector).select("*")) {

            for (TextNode node : el.textNodes()) {
                if (node.text().trim().length() > 0)
                    builder.append(node.text().trim() + "\n");

            }

        }
        return builder.toString();
    }

    public static Collection<String> parseDocumentLinks(Document document, String relative) {
        Set<String> links = new TreeSet<>();
        Elements docLinks = document.select(HREF_SELECTOR);
        docLinks.forEach(new Consumer<Element>() {
            @Override
            public void accept(Element l) {
                URL normalized = normalizeLink(l.attr(HREF_ATTR), relative);
                if (normalized != null)
                    links.add(normalized.toString());
            }
        });

        return ImmutableSet.copyOf(links);
    }

    public static URL normalizeLink(String link, String relative) {
        if (link.startsWith(HASH_STRING)) return null;
        if (link.contains(HASH_STRING))
            link = link.split(HASH_STRING)[0];

        link = link.trim();
        if (EMPTY_STRING.equals(link)) return null;
        String protocol = EMPTY_STRING;
        if (link.contains(COLON + DOUBLE_SLASH)) {
            protocol = link.split(COLON + DOUBLE_SLASH)[0];
            //fixing protocol name  issues
            if (!HTTP_PROTOCOL.equals(protocol) && !HTTPS_PROTOCOL.equals(protocol) && protocol.length() < HTTPS_PROTOCOL.length()) {
                if (HTTP_PROTOCOL.contains(protocol)) {
                    link = link.replace(protocol + COLON + DOUBLE_SLASH, HTTP_PREFIX);
                    protocol = HTTP_PROTOCOL;
                } else if (HTTPS_PROTOCOL.contains(protocol)) {
                    link = link.replace(protocol + COLON + DOUBLE_SLASH, HTTPS_PREFIX);
                    protocol = HTTPS_PROTOCOL;

                }
            }
        }

        URL url = stringToURL(link);
        if (url != null) return url;
        if (link.startsWith(DOUBLE_SLASH)) {
            link = HTTP_PROTOCOL + COLON + link;
            protocol = HTTP_PROTOCOL;
        } else if (link.startsWith(COLON)) {
            link = HTTP_PROTOCOL + link;
            protocol = HTTP_PROTOCOL;
        } else if (link.startsWith(QUESTION_MARK)) {
            link = relative.split("[" + QUESTION_MARK + "]")[0] + link;
            protocol = HTTP_PROTOCOL;
        } else if (link.startsWith(WORLD_WIDE_WEB)) {
            link = HTTP_PROTOCOL + COLON + DOUBLE_SLASH + link;
            protocol = HTTP_PROTOCOL;
        } else if (link.startsWith(SLASH)) {
            URL relativeUrl = stringToURL(relative);
            String port = EMPTY_STRING;
            int relativeUrlPort = relativeUrl.getPort();
            if (relativeUrlPort > 0 && relativeUrlPort != 80 && relativeUrlPort != 443) {
                port = COLON + String.valueOf(relativeUrlPort);
            }
            protocol = relativeUrl.getProtocol();
            link = protocol + COLON + DOUBLE_SLASH + relativeUrl.getHost() + port + link;

        } else if (link.startsWith(PREV_SIGN)) {
            String linkParts[] = link.split("\\.\\./");
            if (linkParts.length > 1) {
                int decreaseCount = linkParts.length - 1;
                String addressPartsMainUrl[] = relative.split("/");
                StringBuilder finalUrl = new StringBuilder();
                for (int k = 0; k < addressPartsMainUrl.length - decreaseCount; k++) {
                    finalUrl.append(addressPartsMainUrl[k] + "/");
                }
                finalUrl.append(linkParts[linkParts.length - 1]);
                url = stringToURL(finalUrl.toString());
                if (url != null) {
                    return url;
                }
            }
        }

        if (EMPTY_STRING.equals(protocol)) {
            link = HTTP_PREFIX + link;
        }


        url = stringToURL(link);
        if (url != null) return url;

        // seems like it is a relative url


        return null;

    }

    public static Collection<String> filterOutByDomain(Collection<String> links, String domain) {
        return filterOutByDomain(links, domain, true);
    }

    public static Collection<String> filterOutByDomain(Collection<String> links, String domain, boolean excludeRoot) {
        final String preparedDomain = domain.replace(WORLD_WIDE_WEB, EMPTY_STRING).toLowerCase();
        return links.stream().map(DocumentUtils::stringToURL).filter(url -> {
            String host = url.getHost().replace(WORLD_WIDE_WEB, EMPTY_STRING).toLowerCase();

            boolean domainsEqual = preparedDomain.equals(host);
            if (!excludeRoot || !domainsEqual)
                return domainsEqual;
            String urlStr = url.toString().toLowerCase();
            urlStr = urlStr.endsWith("/") ? urlStr.substring(0, urlStr.length() - 1) : urlStr;

            return !urlStr.equals(HTTP_PREFIX + preparedDomain) &&
                    !urlStr.equals(HTTP_PREFIX + WORLD_WIDE_WEB + preparedDomain) &&
                    !urlStr.equals(HTTPS_PREFIX + preparedDomain) &&
                    !urlStr.equals(HTTPS_PREFIX + WORLD_WIDE_WEB + preparedDomain);

        }).map(URL::toString).collect(Collectors.toList());
    }

    public static void main(String args[]) throws Exception {

        Connection.Response rep = Jsoup.connect("http://asmarterplanet.com/blog/2009/08/in-dem-video-wird-die-notwendigkeit-fur-mehr-transparenz-in-der.html")
                .header(USER_AGENT_HEADER, USER_AGENT_VALUE).followRedirects(true)
                .timeout(50000)
                .execute();

        Collection<String> links = parseDocumentLinks(rep.parse(), "http://bioengineer.org");
        System.out.println(filterOutByDomain(links, "bioengineer.org"));

    }

    public static URL encodeURL(URL url) {
        try {
            String decodedUrlStr = URLDecoder.decode(url.toString(), "UTF-8");
            URL decodedUrl = new URL(decodedUrlStr);

            URI uri = new URI(decodedUrl.getProtocol(), decodedUrl.getUserInfo(), decodedUrl.getHost(), decodedUrl.getPort(), decodedUrl.getPath(),
                    decodedUrl.getQuery(), decodedUrl.getRef());
            return new URL(uri.toASCIIString());
        } catch (Exception err) {
            //  System.out.println(url);
            // err.printStackTrace();
        }
        return url;
    }

    public static URL stringToURL(String url) {
        try {
            return new URL(url);
        } catch (Exception e) {
            return null;
        }
    }

    private static final String[] possibleFormats = new String[]{"yyyy-MM-dd'T'hh:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:sszzz", "yyyy-MM-dd"};

    public static Date resolveAndGetDate(String firstCandidate, String secondCandidate) {
        if (firstCandidate.length() > 19) {
            firstCandidate = firstCandidate.substring(0, 19) + firstCandidate.substring(19).replace(":", "");
        }
        if (secondCandidate.length() > 19) {
            secondCandidate = secondCandidate.substring(0, 19) + secondCandidate.substring(19).replace(":", "");
        }
        Date firstDate = null;
        Date secondDate = null;
        for (String format : possibleFormats) {
            if (firstDate == null) {
                try {

                    firstDate = new SimpleDateFormat(format).parse(firstCandidate);
                } catch (Exception e) {
                }
            }
            if (secondDate == null) {
                try {

                    secondDate = new SimpleDateFormat(format).parse(secondCandidate);
                } catch (Exception e) {
                }
            }
        }


        if (firstDate == null)

        {
            return secondDate;
        } else if (secondDate == null)

        {
            return firstDate;
        } else

        {
            return secondDate.getTime() < firstDate.getTime() ? secondDate : firstDate;
        }


    }

    public static List<String> fileList(String directory, int count) {
        List<String> fileNames = new ArrayList<>();
        int localCount = 0;
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
            for (Path path : directoryStream) {

                fileNames.add(path.toString());
                if (++localCount >= count) {
                    break;
                }
            }
        } catch (IOException ex) {
        }
        return fileNames;
    }

    public static int fileCount(String directory) {
        List<String> fileNames = new ArrayList<>();
        int count = 0;
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
            return Iterators.size(directoryStream.iterator());
        } catch (IOException ex) {
        }
        return count;
    }

    public static boolean hostExists(String host, int checkTimes) {
        for (int i = 0; i < checkTimes; i++) {
            try {
                InetAddress inetAddress = InetAddress.getLocalHost();


                InetAddress inetAddressArray = InetAddress.getByName("www.google.com");
                return true;
            } catch (UnknownHostException e) {

            }
        }
        return false;
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }
}
