package my.cen.utils;

/**
 * Created  on 9/16/2015.
 */
public class ThreadUtils {
    public static void sleepNoInterrupt(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            // skip
        }
    }
}
