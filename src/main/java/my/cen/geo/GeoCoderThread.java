package my.cen.geo;

import my.cen.service.ElasticSearchService;
import my.cen.utils.DocumentUtils;
import my.cen.utils.ThreadUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.common.collect.ImmutableBiMap;
import org.elasticsearch.common.collect.ImmutableMap;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Created  on 9/19/2015.
 */
public class GeoCoderThread extends Thread {
    private static final Logger logger = Logger.getLogger(GeoCoderThread.class);
    private final Collection<GeoObject> geoCodes;
    private final ElasticSearchService ess;

    public GeoCoderThread(Collection<GeoObject> geoCodes, ElasticSearchService ess) {
        this.geoCodes = geoCodes;
        this.ess = ess;
    }

    @Override
    public void run() {
        while (true) {
            for (GeoObject geo : geoCodes) {
                BoolQueryBuilder q;

                String tag = geo.getName();
                if (tag.contains(" ")) {
                    String es_tag = DocumentUtils.prepareTag(geo.getName());
                    String tagCombined = tag.replace(" ", "").toLowerCase();
                    q = QueryBuilders.boolQuery()
                            .must(QueryBuilders.boolQuery()
                                            .should(prepareSplitQB(tag.split(" ")))
                                            .should(QueryBuilders.termQuery("tags", es_tag))
                                            .should(QueryBuilders.termQuery("tags", tagCombined))
                            );
                } else {
                    q = prepareSingleTermQuery(tag);
                }


                ess.processQueryResults(new String[]{"documents"}, q,  FilterBuilders.missingFilter("geo.point"), (h, t, c) -> processSearchResult(h, t, c, geo)
                        , "img", "url", "header", "doctype", "date", "tags", "geo.point");
            }
            ThreadUtils.sleepNoInterrupt(TimeUnit.MINUTES.toMillis(1));
        }
    }

    private void processSearchResult(SearchHit hit, long totalCount, int currentNumber, GeoObject geoObject) {
        logger.debug(String.format("Processing record %d out of %d for geo object %s", currentNumber, totalCount, geoObject));
        String countryTag = DocumentUtils.prepareTag(geoObject.getCountry());
        String geoTag = DocumentUtils.prepareTag(geoObject.getName());
        Collection<Object> tags = hit.getFields().get("tags").getValues();
        ImmutableMap.Builder<Object, Object> doc = new ImmutableBiMap.Builder<>()
                .put("img", hit.getFields().get("img").value())
                .put("url", hit.getFields().get("url").value())
                .put("header", hit.getFields().get("header").value())
                .put("doctype", hit.getFields().get("doctype").value())
                .put("date", hit.getFields().get("date").value())
                .put("tags", tags);
        if (!hit.getFields().get("tags").getValues().contains(countryTag)) {
            tags.add(countryTag);
        }
        if (!hit.getFields().get("tags").getValues().contains(geoTag)) {
            tags.add(geoTag);
        }

        doc.put("geo.point", geoObject.getLatLong());
        ess.index("documents", "site", hit.getId(), doc.build());
    }


    private BoolQueryBuilder prepareSingleTermQuery(String word) {
        return QueryBuilders.boolQuery()
                .should(QueryBuilders.termQuery("header", word.trim()))
                .should(QueryBuilders.termQuery("tags", word.trim()));
    }

    private BoolQueryBuilder prepareSplitQB(String[] words) {
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        for (String word : words) {
            builder.must(prepareSingleTermQuery(word));
        }
        return builder;
    }
}
