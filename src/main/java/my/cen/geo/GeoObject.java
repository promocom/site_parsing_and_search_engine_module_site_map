package my.cen.geo;

/**
 * Created  on 9/19/2015.
 */
public class GeoObject {
    private final String name;
    private final String country;
    private final String latLong;

    public GeoObject(String name, String country, String latLong) {
        this.name = name;
        this.country = country;
        this.latLong = latLong;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getLatLong() {
        return latLong;
    }

    @Override
    public String toString() {
        return name + " (" + country + ")";
    }
}

