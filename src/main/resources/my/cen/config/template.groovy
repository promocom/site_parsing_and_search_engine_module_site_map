
import my.cen.parser.IParser

public final class Class_$NAME$_ implements IParser {
    void parse(Map<String, Object> map) {
        if (map["init"]) {
            map["site"] = "_$HOST$_"

            map["skipParts"] = "javascript,mailto,.mp4,.pdf,.jpeg,.jpg,.png,.gif,.mp3,.avi,.flw,.fla,upload,authors,rss,tag,forum,print,/stream/,/reporters/,/user/"
        } else {
            map["skip"] = !"article".equals(map["utils"].getAttrValue(map["dom"], "meta[property=\"og:type\"]", "content").trim())
            if (!map["skip"]) {
                map["tags"] = new TreeSet();
                map["header"] = map["utils"].getAttrValue(map["dom"], "meta[property=\"og:title\"]", "content").trim().split("\\|")[0];
                map["img"] = map["utils"].getAttrValue(map["dom"], "meta[property=\"og:image\"]", "content").trim();
                String dateModified = map["utils"].getAttrValue(map["dom"], "meta[property=article:modified_time]", "content").trim();
                String dateCreated = map["utils"].getAttrValue(map["dom"], "meta[property=article:published_time]", "content").trim();
                map["date"] = map["utils"].resolveAndGetDate(dateModified, dateCreated);
                if (map["date"] == null) {
                    map["skip"] = true;
                    return;
                }
                map["tags"].addAll(Arrays.asList(map["utils"].getAttrValue(map["dom"], "meta[name=\"news_keywords\"]", "content").trim().split(",")));
                map["tags"].addAll(map["utils"].getAttrValueList(map["dom"], "meta[name=\"article:tag\"]", "content"));
                map["tags"].addAll(map["utils"].getAttrValueList(map["dom"], "meta[name=\"article:tag\"]", "content"));
                map["tags"].addAll(Arrays.asList(map["utils"].getAttrValue(map["dom"], "meta[name=\"keywords\"]", "content").split(",")));
                map["tags"]=map["utils"].prepareKeywords(map["tags"]);
            }
        }
    }
}