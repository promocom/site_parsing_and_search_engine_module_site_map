<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 1/12/2015
  Time: 3:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE HTML>
<html>

<head>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        div {
            margin: 20px;
        }

        ul {
            list-style-type: none;
            width: 100%;
        }

        a {
            color: black;
        }

        li img {
            float: left;
            margin: 0 15px 0 0;
            padding-right: 5px;
        }

        li p {
            font: 200 12px/1.5 Georgia, Times New Roman, serif;
        }

        li {
            padding: 10px;
            overflow: auto;
        }

        li:hover {
            background: #eee;
            cursor: pointer;
        }
    </style>

</head>

<body>

<div>
    <ul id="items">

    </ul>
</div>
<script src="/ui/js/jquery.js"></script>
<script src="/ui/js/ui2.js"></script>

</body>

</html>
