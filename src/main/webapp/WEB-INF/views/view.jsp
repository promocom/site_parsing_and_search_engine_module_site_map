<%--
  Created by IntelliJ IDEA.
  Date: 9/19/2015
  Time: 5:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>${headerText}</title>

    <link href="/resources/css/styles.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="/resources/css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->
    <link href="/resources/js/leaflet/leaflet.css" rel="stylesheet" type="text/css"/>
    <link href="/resources/js/leaflet/MarkerCluster.Default.css" rel="stylesheet" type="text/css"/>
    <link href="/resources/js/leaflet/MarkerCluster.css" rel="stylesheet" type="text/css"/>


</head>

<body>

<!-- Top line begins -->
<div id="top">
    <div class="wrapper">
        <a href="index.html" title="" class="logo"><img src="/resources/images/logo.png" alt=""/></a>

        <!-- Right top nav -->
        <div class="topNav">
            <ul class="userNav">
                <li><a title="" class="search"></a></li>
                <li><a href="#" title="" class="screen"></a></li>
                <li><a href="#" title="" class="settings"></a></li>
                <li><a href="#" title="" class="logout"></a></li>
                <li class="showTabletP"><a href="#" title="" class="sidebar"></a></li>
            </ul>
            <a title="" class="iButton"></a>
            <a title="" class="iTop"></a>

            <div class="topSearch">
                <div class="topDropArrow"></div>
                <form action="">
                    <input type="text" placeholder="search..." name="topSearch"/>
                    <input type="submit" value=""/>
                </form>
            </div>
        </div>

        <!-- Responsive nav -->
        <ul class="altMenu">
            <li><a href="index.html" title="">Dashboard</a></li>
            <li><a href="ui.html" title="" class="exp">UI elements</a>
                <ul>
                    <li><a href="ui.html">General elements</a></li>
                    <li><a href="ui_icons.html">Icons</a></li>
                    <li><a href="ui_buttons.html">Button sets</a></li>
                    <li><a href="ui_grid.html">Grid</a></li>
                    <li><a href="ui_custom.html">Custom elements</a></li>
                </ul>
            </li>
            <li><a href="forms.html" title="" class="exp">Forms stuff</a>
                <ul>
                    <li><a href="forms.html">Inputs &amp; elements</a></li>
                    <li><a href="form_validation.html">Validation</a></li>
                    <li><a href="form_editor.html">File uploads &amp; editor</a></li>
                    <li><a href="form_wizards.html">Form wizards</a></li>
                </ul>
            </li>
            <li><a href="messages.html" title="">Messages</a></li>
            <li><a href="statistics.html" title="">Statistics</a></li>
            <li><a href="tables.html" title="" class="exp" id="current">Tables</a>
                <ul>
                    <li><a href="tables.html" class="active">Standard tables</a></li>
                    <li><a href="tables_dynamic.html">Dynamic tables</a></li>
                    <li><a href="tables_control.html">Tables with control</a></li>
                    <li><a href="tables_sortable.html">Sortable &amp; resizable</a></li>
                </ul>
            </li>
            <li><a href="other_calendar.html" title="" class="exp">Other pages</a>
                <ul>
                    <li><a href="other_calendar.html">Calendar</a></li>
                    <li><a href="other_gallery.html">Images gallery</a></li>
                    <li><a href="other_file_manager.html">File manager</a></li>
                    <li><a href="other_404.html">Sample error page</a></li>
                    <li><a href="other_typography.html">Typography</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- Top line ends -->


<!-- Sidebar begins -->
<div id="sidebar">
    <div class="mainNav">
        <div class="user">
            <a title="" class="leftUserDrop"><img src="/resources/images/user.png"
                                                  alt=""/><span><strong>3</strong></span></a><span>Eugene</span>
            <ul class="leftUser">
                <li><a href="#" title="" class="sProfile">My profile</a></li>
                <li><a href="#" title="" class="sMessages">Messages</a></li>
                <li><a href="#" title="" class="sSettings">Settings</a></li>
                <li><a href="#" title="" class="sLogout">Logout</a></li>
            </ul>
        </div>

        <!-- Responsive nav -->
        <div class="altNav">
            <div class="userSearch">
                <form action="">
                    <input type="text" placeholder="search..." name="userSearch"/>
                    <input type="submit" value=""/>
                </form>
            </div>

            <!-- User nav -->
            <ul class="userNav">
                <li><a href="#" title="" class="profile"></a></li>
                <li><a href="#" title="" class="messages"></a></li>
                <li><a href="#" title="" class="settings"></a></li>
                <li><a href="#" title="" class="logout"></a></li>
            </ul>
        </div>

        <!-- Main nav -->
        <ul class="nav">
            <li><a href="index.html" title=""><img src="/resources/images/icons/mainnav/dashboard.png" alt=""/><span>Dashboard</span></a>
            </li>
            <li><a href="ui.html" title=""><img src="/resources/images/icons/mainnav/ui.png"
                                                alt=""/><span>UI elements</span></a>
                <ul>
                    <li><a href="ui.html" title=""><span class="icol-fullscreen"></span>General elements</a></li>
                    <li><a href="ui_icons.html" title=""><span class="icol-images2"></span>Icons</a></li>
                    <li><a href="ui_buttons.html" title=""><span class="icol-coverflow"></span>Button sets</a></li>
                    <li><a href="ui_grid.html" title=""><span class="icol-view"></span>Grid</a></li>
                    <li><a href="ui_custom.html" title=""><span class="icol-cog2"></span>Custom elements</a></li>
                </ul>
            </li>
            <li><a href="forms.html" title=""><img src="/resources/images/icons/mainnav/forms.png" alt=""/><span>Forms stuff</span></a>
                <ul>
                    <li><a href="forms.html" title=""><span class="icol-list"></span>Inputs &amp; elements</a></li>
                    <li><a href="form_validation.html" title=""><span class="icol-alert"></span>Validation</a></li>
                    <li><a href="form_editor.html" title=""><span class="icol-pencil"></span>File uploader &amp; WYSIWYG</a>
                    </li>
                    <li><a href="form_wizards.html" title=""><span class="icol-signpost"></span>Form wizards</a></li>
                </ul>
            </li>
            <li><a href="messages.html" title=""><img src="/resources/images/icons/mainnav/messages.png" alt=""/><span>Messages</span></a>
            </li>
            <li><a href="statistics.html" title=""><img src="/resources/images/icons/mainnav/statistics.png"
                                                        alt=""/><span>Statistics</span></a></li>
            <li><a href="tables.html" title="" class="active"><img src="/resources/images/icons/mainnav/tables.png"
                                                                   alt=""/><span>Tables</span></a>
                <ul>
                    <li><a href="tables.html" title=""><span class="icol-frames"></span>Standard tables</a></li>
                    <li><a href="tables_dynamic.html" title=""><span class="icol-refresh"></span>Dynamic table</a></li>
                    <li><a href="tables_control.html" title=""><span class="icol-bullseye"></span>Tables with
                        control</a></li>
                    <li><a href="tables_sortable.html" title=""><span class="icol-transfer"></span>Sortable and
                        resizable</a></li>
                </ul>
            </li>
            <li><a href="other_calendar.html" title=""><img src="/resources/images/icons/mainnav/other.png"
                                                            alt=""/><span>Other pages</span></a>
                <ul>
                    <li><a href="other_calendar.html" title=""><span class="icol-dcalendar"></span>Calendar</a></li>
                    <li><a href="other_gallery.html" title=""><span class="icol-images2"></span>Images gallery</a></li>
                    <li><a href="other_file_manager.html" title=""><span class="icol-files"></span>File manager</a></li>
                    <li><a href="#" title="" class="exp"><span class="icol-alert"></span>Error pages <span
                            class="dataNumRed">6</span></a>
                        <ul>
                            <li><a href="other_403.html" title="">403 error</a></li>
                            <li><a href="other_404.html" title="">404 error</a></li>
                            <li><a href="other_405.html" title="">405 error</a></li>
                            <li><a href="other_500.html" title="">500 error</a></li>
                            <li><a href="other_503.html" title="">503 error</a></li>
                            <li><a href="other_offline.html" title="">Website is offline error</a></li>
                        </ul>
                    </li>
                    <li><a href="other_typography.html" title=""><span class="icol-create"></span>Typography</a></li>
                    <li><a href="other_invoice.html" title=""><span class="icol-money2"></span>Invoice template</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <!-- Secondary nav -->
    <div class="secNav">
        <div class="secWrapper">
            <div class="secTop">
                <div class="balance">
                    <div class="balInfo">Tototal news:<span>&nbsp;</span></div>
                    <div class="balAmount"><span
                            class="balBars"><!--5,10,15,20,18,16,14,20,15,16,12,10--></span><span>${stat.processed.get(stat.processed.size()-1)}</span>
                    </div>
                </div>
                <a href="#" class="triangle-red"></a>
            </div>

            <!-- Sidebar dropdown -->
            <ul class="fulldd">
                <li class="has">
                    <a title="">
                        <span class="icos-money3"></span>
                        Invoices
                        <span><img src="/resources/images/elements/control/hasddArrow.png" alt=""/></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar datepicker -->

            <div class="divider"><span></span></div>
            <div class="sideWidget">
                <div class="inlinedate"></div>
            </div>
            <div class="divider"><span></span></div>
            <div class="tagsinput">
                <c:forEach items="${searchRes.tags}" var="obj">
                    <span class="tag"><a class="tagLink" l="${obj}" href="javascript:">#${obj}</a></span>
                </c:forEach>
            </div>
            <div class="divider"><span></span></div>


            <!-- Sidebar subnav -->
            <ul class="subNav">
                <li><a href="tables.html" title="" class="this"><span class="icos-frames"></span>Standard tables</a>
                </li>
                <li><a href="tables_dynamic.html" title=""><span class="icos-refresh"></span>Dynamic table</a></li>
                <li><a href="tables_control.html" title=""><span class="icos-bullseye"></span>Tables with control</a>
                </li>
                <li><a href="tables_sortable.html" title=""><span class="icos-transfer"></span>Sortable and
                    resizable</a></li>
            </ul>

            <div class="divider"><span></span></div>

            <!-- Sidebar big buttons -->
            <div class="sidePad">
                <a href="#" title="" class="sideB bBlue">Add new session</a>
                <a href="#" title="" class="sideB bRed mt10">Add new session</a>
                <a href="#" title="" class="sideB bGreen mt10">Add new session</a>
                <a href="#" title="" class="sideB bGreyish mt10">Add new session</a>
                <a href="#" title="" class="sideB bBrown mt10">Add new session</a>
            </div>

            <div class="divider"><span></span></div>

            <!-- Sidebar user list -->
            <ul class="userList">
                <li>
                    <a href="#" title="">
                        <img src="/resources/images/live/face1.png" alt=""/>
                        <span class="contactName">
                            <strong>Eugene Kopyov <span>(5)</span></strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_away"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="/resources/images/live/face2.png" alt=""/>
                        <span class="contactName">
                            <strong>Lucy Wilkinson <span>(12)</span></strong>
                            <i>Team leader</i>
                        </span>
                        <span class="status_off"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="/resources/images/live/face3.png" alt=""/>
                        <span class="contactName">
                            <strong>John Dow</strong>
                            <i>PHP developer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
            </ul>

            <div class="divider"><span></span></div>
            <!-- Sidebar tags line -->


            <div class="divider"><span></span></div>
            <!-- Sidebar send message form -->
            <div class="sideWidget">
                <div class="formRow">
                    <textarea rows="8" cols="" name="textarea" placeholder="Your message"></textarea>
                </div>
                <div class="formRow">
                    <input type="submit" class="buttonS bLightBlue" value="Submit button"/>
                </div>
            </div>

            <div class="divider"><span></span></div>

            <!-- Sidebar file tree -->
            <div class="widget">
                <div class="whead">
                    <h6><span class="icon-tree-view"></span>Simple jQuery file tree</h6>
                </div>
                <div class="filetree"></div>
            </div>

            <div class="divider"><span></span></div>

            <!-- Sidebar buttons -->
            <div class="fluid sideWidget">
                <div class="grid6"><input type="submit" class="buttonS bRed" value="Cancel"/></div>
                <div class="grid6"><input type="submit" class="buttonS bGreen" value="Submit"/></div>
            </div>

            <div class="divider"><span></span></div>

        </div>
    </div>
</div>
<!-- Sidebar ends -->


<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle"><span class="icon-screen"></span>Classic standard tables</span>
        <ul class="quickStats">
            <li>
                <a href="" class="blueImg"><img src="/resources/images/icons/quickstats/plus.png" alt=""/></a>

                <div class="floatR"><strong class="blue">5489</strong><span>visits</span></div>
            </li>
            <li>
                <a href="" class="redImg"><img src="/resources/images/icons/quickstats/user.png" alt=""/></a>

                <div class="floatR"><strong class="blue">4658</strong><span>users</span></div>
            </li>
            <li>
                <a href="" class="greenImg"><img src="/resources/images/icons/quickstats/money.png" alt=""/></a>

                <div class="floatR"><strong class="blue">1289</strong><span>orders</span></div>
            </li>
        </ul>
    </div>

    <!-- Breadcrumbs line -->
    <div class="breadLine">
        <div class="bc">
            <ul id="breadcrumbs" class="breadcrumbs">
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="tables.html">Tables</a>
                    <ul>
                        <li><a href="tables_dynamic.html" title="">Dynamic table</a></li>
                        <li><a href="tables_control.html" title="">Tables with control</a></li>
                        <li><a href="tables_sortable.html" title="">Sortable and resizable</a></li>
                    </ul>
                </li>
                <li class="current"><a href="tables.html" title="">Standard tables</a></li>
            </ul>
        </div>

        <div class="breadLinks">
            <ul>
                <li><a href="#" title=""><i class="icos-list"></i><span>Orders</span> <strong>(+58)</strong></a></li>
                <li><a href="#" title=""><i class="icos-check"></i><span>Tasks</span> <strong>(+12)</strong></a></li>
                <li class="has">
                    <a title="">
                        <i class="icos-money3"></i>
                        <span>Invoices</span>
                        <span><img src="/resources/images/elements/control/hasddArrow.png" alt=""/></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- Main content -->
    <div class="wrapper">
        <ul class="middleNavR">
            <li><a href="#" title="Add an article" class="tipN"><img src="/resources/images/icons/middlenav/create.png"
                                                                     alt=""/></a></li>
            <li><a href="#" title="Upload files" class="tipN"><img src="/resources/images/icons/middlenav/upload.png"
                                                                   alt=""/></a></li>
            <li><a href="#" title="Add something" class="tipN"><img src="/resources/images/icons/middlenav/add.png"
                                                                    alt=""/></a></li>
            <li><a href="#" title="Messages" class="tipN"><img src="/resources/images/icons/middlenav/dialogs.png"
                                                               alt=""/></a><strong>8</strong></li>
            <li><a href="#" title="Check statistics" class="tipN"><img src="/resources/images/icons/middlenav/stats.png"
                                                                       alt=""/></a></li>
        </ul>


        <!-- Table inside tabs -->
        <div class="widget rightTabs tableTabs">
            <div class="whead"><h6>${headerText} Total results:${searchRes.total}</h6></div>
            <div class="tabs">
                <ul>
                    <li><a href="#ttab_map">Map view</a></li>
                    <li><a href="#ttab1">Table view</a></li>
                </ul>
                <div id="ttab1">
                    <table cellpadding="0" cellspacing="0" width="100%" class="tDefault checkAll tMedia" id="checkAll">
                        <thead>
                        <tr>

                            <td width="50">Image</td>
                            <td class="sortCol">
                                <div>Description<span></span></div>
                            </td>
                            <td class="sortCol" width="130">
                                <div>Date<span></span></div>
                            </td>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">

                                <div class="tPages">
                                    <ul class="pages">
                                        <li class="prev">
                                        <li><a id="nextBtn" href="javascript:" title="">More</a></li>


                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                        <tbody id="news">
                        <c:forEach items="${searchRes.data}" var="obj">
                            <tr>
                                <td><a href="${obj.url}" title="" class="lightbox"><img src="${obj.img}" width="100px"
                                                                                        alt=""/></a></td>
                                <td class="textL"><a href="${obj.url}" title="">${obj.header}</a></td>
                                <td>${obj.date}</td>

                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
                <div id="ttab_map" style="height: 900px">

                </div>
            </div>
        </div>


    </div>
    <!-- Main content ends -->

</div>
<!-- Content ends -->


</body>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="/resources/js/plugins/charts/excanvas.min.js"></script>
<script type="text/javascript" src="/resources/js/plugins/charts/jquery.flot.js"></script>
<script type="text/javascript" src="/resources/js/plugins/charts/jquery.flot.orderBars.js"></script>
<script type="text/javascript" src="/resources/js/plugins/charts/jquery.flot.pie.js"></script>
<script type="text/javascript" src="/resources/js/plugins/charts/jquery.flot.resize.js"></script>
<script type="text/javascript" src="/resources/js/plugins/charts/jquery.sparkline.min.js"></script>

<script type="text/javascript" src="/resources/js/plugins/tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/resources/js/plugins/tables/jquery.sortable.js"></script>
<script type="text/javascript" src="/resources/js/plugins/tables/jquery.resizable.js"></script>

<script type="text/javascript" src="/resources/js/plugins/forms/jquery.autosize.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.uniform.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.inputlimiter.min.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.autotab.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.select2.min.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.dualListBox.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.cleditor.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.ibutton.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="/resources/js/plugins/forms/jquery.validationEngine.js"></script>

<script type="text/javascript" src="/resources/js/plugins/uploader/plupload.js"></script>
<script type="text/javascript" src="/resources/js/plugins/uploader/plupload.html4.js"></script>
<script type="text/javascript" src="/resources/js/plugins/uploader/plupload.html5.js"></script>
<script type="text/javascript" src="/resources/js/plugins/uploader/jquery.plupload.queue.js"></script>

<script type="text/javascript" src="/resources/js/plugins/wizards/jquery.form.wizard.js"></script>
<script type="text/javascript" src="/resources/js/plugins/wizards/jquery.validate.js"></script>
<script type="text/javascript" src="/resources/js/plugins/wizards/jquery.form.js"></script>

<script type="text/javascript" src="/resources/js/plugins/ui/jquery.collapsible.min.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.breadcrumbs.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.tipsy.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.progress.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.timeentry.min.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.colorpicker.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.jgrowl.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.fancybox.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.fileTree.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.sourcerer.js"></script>

<script type="text/javascript" src="/resources/js/plugins/others/jquery.fullcalendar.js"></script>
<script type="text/javascript" src="/resources/js/plugins/others/jquery.elfinder.js"></script>

<script type="text/javascript" src="/resources/js/plugins/forms/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/resources/js/plugins/ui/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="/resources/js/files/bootstrap.js"></script>
<script type="text/javascript" src="/resources/js/files/functions.js"></script>
<script type="text/javascript" src="/resources/js/files/functions.js"></script>
<script type="text/javascript" src="/resources/js/leaflet/leaflet.js"></script>
<script type="text/javascript" src="/resources/js/leaflet/leaflet.markercluster.js"></script>


<script type="application/javascript">
    var sid = '${searchRes.sid}';
    var exp = '${searchRes.exp}';
    var dt = '${searchRes.dt}' === '' ? undefined : '${searchRes.dt}';

    $(function () {
        var map = L.map('ttab_map').setView([51.505, -0.09], 2);
        L.tileLayer("http://otile1.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpg",
                {
                    attribution: "Data, imagery and map information provided by MapQuest, OpenStreetMap <http://www.openstreetmap.org/copyright> and contributors, ODbL",
                    maxZoom: 18,
                    minZoom: 2,

                }).
                addTo(map);
        var load = function (exp, next, date) {
            var url = "/api/es/search?exp=" + exp + "&by=date";
            if (next != undefined) {
                url += "&sid=" + next
            }
            if (date != undefined) {
                url += "&date=" + date
            }
            $.ajax({
                url: url,
                type: "GET",
                datatype: 'json',
                async: true,
                success: function (result) {


                    if (result["data"] != undefined) {

                        result["data"].forEach(function (a) {

                            $("#news").append('<tr><td><a href="' + a.url + '" title="" class="lightbox"><img src="' + a.img + '" width="100px" alt=""/></a></td><td class="textL"><a href="' + a.url + '" title="">' + a.header + '</a></td><td>' + a.date + '</td></tr>');
                        });
                    }
                    sid = result["sid"];
                }
            });
        };


        $(".tagLink").click(function () {
            document.location = ("/?keywords=" + $(this).attr("l")) + (dt === undefined ? "" : "&date=" + dt);

        });
        var defDate = dt == undefined ? new Date() : new Date(dt);
        // Datepicker
        $('.inlinedate').datepicker({
            inline: true,
            altField: "#actualDate",
            defaultDate: defDate,
            onSelect: function (dateText) {
                document.location = "/?keywords=" + exp + "&date=" + dateText;
            },
            showOtherMonths: true,
            maxDate: new Date()
        });
        $("#nextBtn").click(function () {
            load(exp, sid, dt);
        });
    })
</script>
</html>
